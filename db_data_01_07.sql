-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: evangelistadesouza
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actuations`
--

DROP TABLE IF EXISTS `actuations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actuations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `actuations_slug_unique` (`slug`),
  KEY `actuations_slug_index` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actuations`
--

LOCK TABLES `actuations` WRITE;
/*!40000 ALTER TABLE `actuations` DISABLE KEYS */;
INSERT INTO `actuations` VALUES (1,'Direito Trabalhista','direito-trabalhista','actuation/XEVfuX4MTmmr3hkZaHOCVmPcBe9zN1K4DQHfkVMo.webp','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. <br> <h2>Contensioso Trabalhista</h2> <br> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br> Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus.','2018-10-10 15:48:38','2018-10-29 15:09:51'),(2,'Direito Empresarial','direito-empresarial','actuation/4suOPwSXI910gOqwF33SfiyD4x703ekWTu5iJ7pE.jpeg','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. <br> <h2>Fusões e Aquisições</h2> <br> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br> Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus.','2018-10-10 15:48:38','2018-10-29 15:02:19'),(4,'Direito Previdenciário','direito-previdenciario','actuation/direito-previdenciario.jpg','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. <br/> <h2>Direito Previdenciário</h2> <br/> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br/> Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus.','2018-10-10 15:48:38','2018-10-10 15:48:38'),(5,'Direito Imobiliário','direito-imobiliario','actuation/qBwwbhSvkBdNz74zckj1H0s8I0WbzS2tpOIhluBX.jpeg','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. <br> <h2>Direito Imobiliáio</h2> <br> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br> Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus.','2018-10-10 15:48:38','2018-10-29 15:03:49'),(7,'Direito Ambiental','direito-ambiental','actuation/DRMoaoff3iJSJHib0zhXajvwZsMjzYH0jSWsL7kF.jpeg','Direito Ambiental','2018-10-29 15:05:43','2018-10-29 15:05:43'),(8,'Direito Tributário','direito-tributario','actuation/3v3jWN6N3ODStp1IkwUG3xXrzU3qOuQwR2dsFPFR.jpeg','Direito Tributário','2018-10-29 15:07:40','2018-10-29 15:07:40');
/*!40000 ALTER TABLE `actuations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tags` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `blogs_slug_unique` (`slug`),
  KEY `blogs_slug_index` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blogs`
--

LOCK TABLES `blogs` WRITE;
/*!40000 ALTER TABLE `blogs` DISABLE KEYS */;
/*!40000 ALTER TABLE `blogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `configs`
--

DROP TABLE IF EXISTS `configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `config_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `config_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `config_keywords` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `config_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `config_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `config_cellphone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `config_information` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_proprietary` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redesocial_facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redesocial_instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redesocial_twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configs`
--

LOCK TABLES `configs` WRITE;
/*!40000 ALTER TABLE `configs` DISABLE KEYS */;
INSERT INTO `configs` VALUES (1,'Evangelista de Souza Advogados','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt quasi, optio placeat, incidunt libero corrupti.','evangelista, souza, advogados, evangelistadesouza, ribeirao, preto, ribeirao preto, sao paulo, oab, sao, paulo','contato@evangelistadesouza.com.br','(16) 9724-1597','(16) 3421-4983','Informações Complementares','Evangelista de Souza Advogados','André Evangelista','Rua Elias Farah Badra, 714 - Nova Ribeirânia','Ribeirão Preto','São Paulo','BR','#','@evangelistadesouzaadvogados','#','2018-10-10 15:48:38','2018-10-29 13:43:14');
/*!40000 ALTER TABLE `configs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2017_11_26_130748_create_pages_table',1),(4,'2017_11_27_140804_create_configs_table',1),(5,'2018_04_05_110904_create_offices_table',1),(6,'2018_04_05_110905_create_office_photos_table',1),(7,'2018_04_06_103248_create_actuations_table',1),(8,'2018_04_18_073850_create_sliders_table',1),(9,'2018_04_18_101225_create_blogs_table',1),(10,'2018_04_20_082350_create_reviews_table',1),(11,'2018_04_20_110852_create_clients_table',1),(12,'2018_04_20_110852_create_professionals_table',1),(13,'2018_04_28_144623_create_pre_registrations_table',1),(14,'2018_05_02_180927_create_newsletters_table',1),(15,'2018_05_02_185022_create_newsletter_confirmations_table',1),(16,'2018_05_04_161210_create_table_reservations',1),(17,'2018_05_04_162101_create_table_reservation_details',1),(18,'2018_05_18_222042_create_reservation_interests_table',1),(19,'2018_10_03_155606_add_column_redesociais_table_configs',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter_confirmations`
--

DROP TABLE IF EXISTS `newsletter_confirmations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter_confirmations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `newsletter_id` int(10) unsigned NOT NULL,
  `code` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `newsletter_confirmations_newsletter_id_foreign` (`newsletter_id`),
  KEY `newsletter_confirmations_code_index` (`code`),
  CONSTRAINT `newsletter_confirmations_newsletter_id_foreign` FOREIGN KEY (`newsletter_id`) REFERENCES `newsletters` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter_confirmations`
--

LOCK TABLES `newsletter_confirmations` WRITE;
/*!40000 ALTER TABLE `newsletter_confirmations` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter_confirmations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletters`
--

DROP TABLE IF EXISTS `newsletters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('waiting','active','cancel') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'waiting',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletters`
--

LOCK TABLES `newsletters` WRITE;
/*!40000 ALTER TABLE `newsletters` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `office_photos`
--

DROP TABLE IF EXISTS `office_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `office_photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `office_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `office_photos_office_id_foreign` (`office_id`),
  CONSTRAINT `office_photos_office_id_foreign` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `office_photos`
--

LOCK TABLES `office_photos` WRITE;
/*!40000 ALTER TABLE `office_photos` DISABLE KEYS */;
/*!40000 ALTER TABLE `office_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offices`
--

DROP TABLE IF EXISTS `offices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offices`
--

LOCK TABLES `offices` WRITE;
/*!40000 ALTER TABLE `offices` DISABLE KEYS */;
INSERT INTO `offices` VALUES (1,'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt quasi, optio placeat, incidunt libero corrupti. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt quasi, optio placeat, incidunt libero corrupti.','2018-10-10 15:48:38','2018-10-10 15:48:38');
/*!40000 ALTER TABLE `offices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pre_registrations`
--

DROP TABLE IF EXISTS `pre_registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pre_registrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` enum('anunciar','comprar','regularizar') COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('waiting','accept','reject') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'waiting',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pre_registrations`
--

LOCK TABLES `pre_registrations` WRITE;
/*!40000 ALTER TABLE `pre_registrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `pre_registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professionals`
--

DROP TABLE IF EXISTS `professionals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professionals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `oab` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professionals`
--

LOCK TABLES `professionals` WRITE;
/*!40000 ALTER TABLE `professionals` DISABLE KEYS */;
/*!40000 ALTER TABLE `professionals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation_details`
--

DROP TABLE IF EXISTS `reservation_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reservation_id` int(10) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'reservations/image.png',
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reservation_details_reservation_id_foreign` (`reservation_id`),
  CONSTRAINT `reservation_details_reservation_id_foreign` FOREIGN KEY (`reservation_id`) REFERENCES `reservations` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation_details`
--

LOCK TABLES `reservation_details` WRITE;
/*!40000 ALTER TABLE `reservation_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservation_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation_interests`
--

DROP TABLE IF EXISTS `reservation_interests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation_interests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reservation_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `visualized` tinyint(1) NOT NULL DEFAULT '0',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reservation_interests_reservation_id_foreign` (`reservation_id`),
  CONSTRAINT `reservation_interests_reservation_id_foreign` FOREIGN KEY (`reservation_id`) REFERENCES `reservations` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation_interests`
--

LOCK TABLES `reservation_interests` WRITE;
/*!40000 ALTER TABLE `reservation_interests` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservation_interests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservations`
--

DROP TABLE IF EXISTS `reservations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pre_registration_id` int(10) unsigned DEFAULT NULL,
  `propietario` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fazenda` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `matricula` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `municipio` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `municipio_cartorio` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bioma` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `situacao` enum('regular','irregular') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'regular',
  `georeferencia` enum('sim','nao') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'nao',
  `inscricao_car` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tamanho_area` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area_tipo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reservations_pre_registration_id_foreign` (`pre_registration_id`),
  CONSTRAINT `reservations_pre_registration_id_foreign` FOREIGN KEY (`pre_registration_id`) REFERENCES `pre_registrations` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservations`
--

LOCK TABLES `reservations` WRITE;
/*!40000 ALTER TABLE `reservations` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `charge` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sliders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sliders`
--

LOCK TABLES `sliders` WRITE;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` VALUES (1,'Diversas áreas do Direito','#','/sliders/sem_image.png','2018-10-10 15:48:38','2018-10-10 15:48:38');
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` enum('admin','moder') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Leonardo Augusto','contato@innsystem.com.br','$2y$10$u8yEmh5KmhdT0rKQht/0Ve1AsO1NnvUVYzSVx61i6/HDfj6BWeKnq','admin','mac0qmTGTC','2018-10-10 15:48:38','2018-10-10 15:48:38'),(2,'André Evangelista','contato@evangelistadesouza.com.br','$2y$10$Gyw/AECfEcVlBVUo60YW6uYOxbgtLtumV7IbDrH9Z61pVd/C7xa3m','admin','N92pO5qzUP','2018-10-10 15:48:38','2018-10-10 15:48:38'),(3,'Ana Laura','analauraboldrin@hotmail.com','$2y$10$rLhpXzOmFQlTdRdZJFMPG.D4zCgPMAo3ONkABGTzPYBTUQGx29ip2','admin','V4y9EYeVT8','2018-10-10 15:48:38','2018-10-29 13:09:05');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-01 10:28:07
