
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('vue2-animate/dist/vue2-animate.min.css');
require('animate.css');

import Vue from 'vue'

// Bootstrap Vue
import 'bootstrap-vue/dist/bootstrap-vue.css'
import {Table, Pagination} from 'bootstrap-vue/src/components'
Vue.use(Table);
Vue.use(Pagination);

// Methods
import VueToastr from '@deveodk/vue-toastr'
import '@deveodk/vue-toastr/dist/@deveodk/vue-toastr.css'
Vue.use(VueToastr, {
    defaultPosition: 'toast-top-right',
    defaultType: 'info',
    defaultTimeout: 1500,
})

// Variaveis
Vue.prototype.$loading = (time = 1000) => {
    return new Promise(function (resolve, reject) {
        $('.loading').fadeIn(500, () => {
            setTimeout(() => {
                $('.loading').fadeOut()
                resolve(true)
            }, time)
        });
    })
}

// Componentes
import FormReserva from './components/FormReserva'
import ListaReservas from './components/ListaReservas'
import FormContato from './components/FormContato'
import FormNewsletter from './components/FormNewsletter'
import ManagerNewsletter from './components/ManagerNewsletter'
import ManageReservations from './components/ManageReservations'

import FrontListReservations from './front-end/ListReservations'
import FrontFormInteresse from './front-end/FormInteresse'

const app = new Vue({
	name: 'EvangelistadeSouza',
    el: '#app',
    components: {
    	FormReserva,
        ListaReservas,
        FormContato,
        FormNewsletter,
        ManagerNewsletter,
        ManageReservations,

        FrontListReservations,
		FrontFormInteresse
    },

    mounted() {
	    $(document).ready(function() {
	        let images = $('.thumbnail');
	        images.each(function() {
	          $(this).attr('onerror', this.src='/placeholder.png');
            });
        });
    }
});
