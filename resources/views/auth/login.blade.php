<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Kode is a Premium Bootstrap Admin Template, It's responsive, clean coded and mobile friendly">
  <meta name="keywords" content="bootstrap, admin, dashboard, flat admin template, responsive," />
  <title>Área Restrita - {{ isset($configs->config_title) ? $configs->config_title : 'Evangelista de Souza' }}</title>

  <!-- ========== Css Files ========== -->
  <link href="/backend/css/root.css?1" rel="stylesheet">
  <style type="text/css">
    body{background: #F5F5F5;}
  </style>
  </head>
  <body>


    <div class="login-form">
      <a href="/dashboard"><img src="{{ asset('logo_preto.png') }}" alt="{{ isset($configs->config_title) ? $configs->config_title : 'Evangelista de Souza' }}" class="img-responsive" style="width:80%; margin:0 auto;"></a>
        <br class="clearfix">
      <form method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="top">
          <h1>Área Restrita</h1>
        </div>
        <div class="form-area">
          <div class="group{{ $errors->has('email') ? ' has-error' : '' }}">
            <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus class="form-control">
            <i class="fa fa-user"></i>
          </div>
          
          <div class="group{{ $errors->has('password') ? ' has-error' : '' }}">
            <input id="password" type="password" name="password" required  class="form-control">
            <i class="fa fa-key"></i>
          </div>


          @if ($errors->has('email'))
          <div class="alert alert-danger">
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            </div>
            @endif
            
          @if ($errors->has('password'))
          <div class="alert alert-danger">
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            </div>
            @endif


          <div class="checkbox checkbox-primary">
            <input id="checkbox101" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
            <label for="checkbox101"> Lembrar-me</label>
          </div>
          <button type="submit" class="btn btn-default btn-block">Logar</button>
        </div>
      </form>
      <div class="footer-links row">
        <div class="col-xs-12 text-right"><a href="{{ route('password.request') }}"><i class="fa fa-lock"></i> Esqueceu sua senha?</a></div>
      </div>
    </div>

</body>
</html>