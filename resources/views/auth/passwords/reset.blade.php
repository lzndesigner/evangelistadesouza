<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Kode is a Premium Bootstrap Admin Template, It's responsive, clean coded and mobile friendly">
  <meta name="keywords" content="bootstrap, admin, dashboard, flat admin template, responsive," />
  <title>Área Restrita - {{ isset($configs->config_title) ? $configs->config_title : 'Meu site' }}</title>

  <!-- ========== Css Files ========== -->
  <link href="/backend/css/root.css" rel="stylesheet">
  <style type="text/css">
  body{background: #F5F5F5;}
  </style>
</head>
<body>

    <div class="login-form">
        <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}
            <div class="top">
              <h1>{{ isset($configs->config_title) ? $configs->config_title : 'Meu site' }}</h1>
              <h4>Recuperar Senha</h4>
          </div>
          <div class="form-area">
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="group{{ $errors->has('email') ? ' has-error' : '' }}">
             <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required placeholder="E-mail de Acesso" autofocus>
             <i class="fa fa-user"></i>
             @if ($errors->has('email'))
             <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>

        <div class="group{{ $errors->has('password') ? ' has-error' : '' }}">
            <input id="password" type="password" class="form-control" name="password" required placeholder="*******">
            <i class="fa fa-key"></i>
            @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>

        <div class="group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="*******">
            <i class="fa fa-key"></i>
            @if ($errors->has('password_confirmation'))
            <span class="help-block">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
            @endif
        </div>

        <button type="submit" class="btn btn-default btn-block">
            Resetar Senha
        </button>


    </div><!-- form-area -->
</form>

<div class="footer-links row">
    <div class="col-xs-6"><a href="{{ route('login') }}"><i class="fa fa-sign-in"></i> Acessar Conta</a></div>
</div>
</div>

</body>
</html>