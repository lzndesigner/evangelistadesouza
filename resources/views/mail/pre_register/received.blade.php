@component('vendor.mail.html.message')
# Pre registro recebido!

Acabamos de receber um pedido via formulário.

<ul>
	<li>Nome completo: {{ $dados->name }}</li>
	<li>Email: {{ $dados->email }}</li>
	<li>Telefone/Celular: {{ $dados->phone }}</li>
	<li>Cidade/Estado: {{ $dados->city }} / {{ $dados->state}}</li>
	<li>Assunto: {{ $dados->subject }}</li>
</ul>

@if($dados->note !== null)
### {{ $dados->note }}
@endif

Obrigado,<br>
{{ config('app.name') }}
@endcomponent
