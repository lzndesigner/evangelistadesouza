@component('mail::message')
# Seja bem vindo

Você confirmou sua inscrição, você pode cancelar sua inscrição a qualquer instante

@component('mail::button', ['url' => url('services/newsletter/confirm/' . base64_encode($dados->confirmation->code) . '?action=false'), 'color' => 'red'])
Clicando aqui
@endcomponent

Obrigado,<br>
{{ config('app.name') }}
@endcomponent
