@component('vendor.mail.html.message')
# Confirme sua inscrição!

Tem certeza que gostaria de receber noticiais via e-mail?
@component('mail::button', ['url' => url('services/newsletter/confirm/' . base64_encode($dados->code) . '?action=true')])
    Sim, quero receber!
@endcomponent

@component('mail::button', ['url' => url('services/newsletter/confirm/' . base64_encode($dados->code) . '?action=false'), 'color' => 'red'])
    Não, quero cancelar!
@endcomponent

Obrigado,<br>
{{ config('app.name') }}
@endcomponent
