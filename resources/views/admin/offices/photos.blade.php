@extends('admin.base')
@section('title', 'Galeria do Escritório')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  @include('elements.messages')

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
           @yield('title')
         </div>

         <div class="panel-body">




          <div class="row">
            <div class="col-xs-12 col-md-12">

              <span class="btn btn-success fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Selecionar Fotos</span>
                <!-- The file input field used as target for the file upload widget -->
                <input id="fileupload" type="file" name="documento" data-token="{!! csrf_token() !!}" data-officeid="{{ $offices->id }}">
              </span>
              <br>
              <br>
              <!-- The global progress bar -->
              <div id="progress" class="progress">
                <div class="progress-bar progress-bar-success"></div>
              </div>
              <!-- The container for the uploaded files -->
              <div id="files" class="files"></div>

            </div>
            <hr class="invisible clearfix">
            <div class="col-xs-12 col-md-12">
              <table class="table table-bordered table-striped table-hover">
                <thead>
                  <th>Imagem</th>
                  <th>Enviado em</th>
                  <th>Ações</th>
                </thead>
                <tbody>
                  @foreach($office->photos as $photo)
                  <tr>
                    <td><img src="/storage/{!! $photo->name !!}" alt="{!! $photo->name !!}" class="img-rounded" style="width:90px;"></td>
                    <td>{!! $photo->created_at !!}</td>
                    <td>
                      <a href="{!! route('office-download', [$office->id, $photo->id]) !!}" class="btn btn-xs btn-success"><i class="fa fa-download"></i></a>
                      <a href="{!! route('office-destroy', [$office->id, $photo->id]) !!}" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>


        </div><!-- col-md-11 -->

        <div class="row">
          <hr class="invisible clearfix">
          <div class="col-md-6">
            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>Salvar</button>
            <a href="{{ url('/dashboard') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> Cancelar</a>
          </div><!-- col-md-6 -->
        </div>

      </div><!-- panel-body -->


    </div><!-- panel-default -->
  </div><!-- col-md-12 -->


</div><!-- row -->
</div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
<link rel="stylesheet" href="/bower_components/blueimp-file-upload/css/jquery.fileupload.css">
@endsection

@section('jsPage')
<!-- ================================================
Bootstrap WYSIHTML5
================================================ -->
<!-- main file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js"></script>
<!-- bootstrap file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<!-- ================================================
Summernote
================================================ -->
<script type="text/javascript" src="/backend/js/summernote/summernote.min.js"></script>

<script>
  /* BOOTSTRAP WYSIHTML5 */
  $('.textarea').wysihtml5();

  /* SUMMERNOTE*/
  $(document).ready(function() {
    $('.summernote').summernote();
  });
</script>

<!-- Query String ToSlug - Transforma o titulo em URL amigavel sem acentos ou espaço -->
<script type="text/javascript" src="/backend/js/jquery.stringToSlug.min.js"></script>
<script type="text/javascript">
  $('input[name="title"]').stringToSlug({
    setEvents: 'keyup keydown blur',
    getPut: 'input[name="url"]',
    space: '-',
    replace: '/\s?\([^\)]*\)/gi',
    AND: 'e'
  });
</script>

<script src="/bower_components/blueimp-file-upload/js/vendor/jquery.ui.widget.js"></script>
<script src="/bower_components/blueimp-file-upload/js/jquery.fileupload.js"></script>
<script>
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });


  ;(function($){
    'use strict';

    $(document).ready(function(){
      //  
      var $fileupload = $('#fileupload');
      $fileupload.fileupload({
        url: '/dashboard/office-upload',
        formData: {_token: $fileupload.data('token'), officeid: $fileupload.data('officeid')},
        dataType: 'json',
        progressall: function (e, data) {
          var progress = parseInt(data.loaded / data.total * 100, 10);
          $('#progress .progress-bar').css(
            'width',
            progress + '%'
            );
          console.log('teste 3');
        }
      }).bind('fileuploadstop', function (e, data) {
        $('#progress .progress-bar').css(
          'width', '0%'
          );
        location.reload();
      });
      //
    });
  })(window.jQuery);
</script>
@endsection



