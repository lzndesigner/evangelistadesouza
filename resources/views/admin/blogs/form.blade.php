{!! csrf_field() !!}

<div class="row">
  <div class="col-md-2">
    <img src="/storage/{{ $blog->image ?? old('image') }}" alt="Notícia" class="img-thumbnail">    
  </div>
  <div class="col-md-10">
    <div class="form-group @if ($errors->has('image')) has-error @endif">
      <label for="image" class="form-label">Imagem da Notícia</label>
      <input type="file" class="form-control" id="image" name="image" value="{{ $blog->image ?? old('image') }}">
      @if ($errors->has('image'))
      <span class="help-block">
        <strong>{{ $errors->first('image') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div>
</div>

<hr class="invisible">

<div class="form-group @if ($errors->has('title')) has-error @endif">
  <label for="title" class="form-label">Título da Notícia</label>
  <input type="text" class="form-control" id="title" name="title" value="{{ $blog->title ?? old('title') }}" placeholder="Título da Notícia" autofocus>
  @if ($errors->has('title'))
  <span class="help-block">
    <strong>{{ $errors->first('title') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group @if ($errors->has('slug')) has-error @endif">
  <label for="slug" class="form-label">URL Amigável</label>
  <input type="text" class="form-control" id="slug" name="slug" readonly value="{{ $blog->slug ?? old('slug') }}" placeholder="URL Amigável">
  <span class="help-block">* É preenchido automaticamente.</span>
  @if ($errors->has('slug'))
  <span class="help-block">
    <strong>{{ $errors->first('slug') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group @if ($errors->has('tags')) has-error @endif">
  <label for="tags" class="form-label">Tags relacionadas</label>
  <input type="text" class="form-control" id="tags" name="tags" value="{{ $blog->tags ?? old('tags') }}" placeholder="Tags relacionadas">
  @if ($errors->has('tags'))
  <span class="help-block">
    <strong>{{ $errors->first('tags') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group @if ($errors->has('body')) has-error @endif">
  <label for="body" class="form-label">Conteúdo da Notícia</label>
  <textarea name="body" id="body" cols="30" rows="10" class="form-control textarea" placeholder="Conteúdo...">{{ $blog->body ??  old('body') }}</textarea>
  @if ($errors->has('body'))
  <span class="help-block">
    <strong>{{ $errors->first('body') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="row">
  <div class="col-md-6">
    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>Salvar</button>
    <a href="{{ route('blogs.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i>Cancelar</a>
  </div><!-- col-md-6 -->
</div>