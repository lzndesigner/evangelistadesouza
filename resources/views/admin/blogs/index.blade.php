@extends('admin.base')
@section('title', 'Páginas de Notícias')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard/') }}">Inicio</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  @include('elements.messages')

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            Lista de @yield('title')
          </div>

          <div class="panel-body">
            <div class="table-responsive">
              @if(count($blogs) > 0)
              <table class="table table-hover table-bordered table-striped" id="blogs-table">
                <thead>
                  <tr>
                    <td width="5%"><a href="{{ Request::fullUrlWithQuery(['sort' => 'id']) }}">#</a></td>
                    <td width="6%">Imagem</td>
                    <td><a href="{{ Request::fullUrlWithQuery(['sort' => 'title']) }}">Title</a></td>
                    <td width="14%"><a href="{{ Request::fullUrlWithQuery(['sort' => 'created_at']) }}">Criação</a></td>
                    <td width="14%"><a href="{{ Request::fullUrlWithQuery(['sort' => 'updated_at']) }}">Alteração</a></td>
                    <td width="15%" class="text-right">Ações</td>
                  </tr>
                </thead>
                <tbody>
                  @foreach($blogs as $blog)
                  <tr id="row-{{ $blog->id }}">
                    <td>
                      <div class="checkbox">
                        <input class="blogs" id="checkbox{{ $blog->id }}" data-id="{{ $blog->id }}" type="checkbox">
                        <label for="checkbox{{ $blog->id }}">
                          <b>{{ $blog->id }}</b>
                        </label>
                      </div>
                    </td>
                    <td><img src="/storage/{{ $blog->image ?? old('image') }}" alt="Notícia" class="img-thumbnail"> </td>
                    <td>{{ $blog->title }}</td>
                    <td>{{ \Carbon\Carbon::parse($blog->created_at)->format('d/m/y H:i')  }}</td>
                    <td>{{ \Carbon\Carbon::parse($blog->updated_at)->format('d/m/y H:i') }}</td>
                    <td class="text-right">
                      <a href="{{ route('blogs.show', $blog->id) }}" class="btn btn-xs btn-default btn-icon" data-toggle="tooltip" data-original-title="Detalhes"><i class="fa fa-book"></i></a>
                      <a href="{{ route('blogs.edit', $blog->id) }}" class="btn btn-xs btn-primary btn-icon" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-edit"></i></a>
                      <a href="#" class="btn btn-xs btn-danger btn-icon btn-delete" data-id="{{ $blog->id }}" data-toggle="tooltip" data-original-title="Remover"><i class="fa fa-remove"></i></a></li>
                    </td>
                  </tr>
                  @endforeach

                </tbody>
              </table>
              @else
              <div class="alert alert-info">Nenhuma notícia cadastrada.</div>
              @endif
              <hr>

            </div><!-- table-responsive -->
          </div><!-- panel-body -->

          <div class="row">
            <div class="col-md-6">
              <a href="{{ route('blogs.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>Novo</a>
              <a href="#" class="btn btn-sm btn-danger btn-remove-all"><i class="fa fa-remove"></i>Remover selecionados</a>
            </div><!-- col-md-6 -->

            <div class="col-md-6 text-right">
              {{ $blogs->links() }}
            </div><!-- col-md-6 -->
          </div>
          

        </div><!-- panel-default -->
      </div><!-- col-md-12 -->
      

    </div><!-- row -->
  </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection


@section('cssPage')
@endsection

@section('jsPage')
<script>
  $(function() {
    let btn = $('.btn-delete');
    let btnAll = $('.btn-remove-all');

    function remove(id) {
      let tr = $('#blogs-table #row-' + id);
      let request = $.ajax({
       url: '/dashboard/blogs/' + id,
       type: 'DELETE',
       statusCode: {
         204: function() {
           tr.fadeOut();
         },
         403: function() {
           let copy = tr.innerHTML;

           tr.html('').append('<td colspan="7">Essa postagem não foi removida.</td>')
           .css({'background-color': 'red','color': 'white', 'text-align': 'center'});
         }
       }
     });
      return request;
    }

    btn.on('click', function(e) {
      e.preventDefault();
      let action = $(e.currentTarget);
      let request = remove(action.data('id'));
      request.fail(function() {
        window.alert('Não foi possivel remover a postagem')
      });
    })

    btnAll.on('click', function(e) {
      e.preventDefault();

      let checkbox = $('input:checkbox[class="blogs"]:checked');

      if(checkbox.length > 0 && window.confirm('Gostaria de remover a(s) ' + checkbox.length + ' selecionada(s) postagem(ns)?')) {
          //
          checkbox.each(function(index, el) {
            let id = $(el).data('id')
            let request = remove(id);
          })
        }

      })
  })
</script>
@endsection