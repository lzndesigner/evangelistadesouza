@extends('admin.base')
@section('title', 'Detalhes da Notícia')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') - {{ $blog->title }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('blogs.index') }}">Página de Notícias</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End blog Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            <h2>{{ $blog->title }}</h2>
          </div>

          <div class="panel-body">

            <img src="/storage/{{ $blog->image ?? old('image') }}" alt="Notícias" class="img-thumbnail">

            <p><small>Criado em {{ $blog->created_at->format('d/m/Y H:i:s') }} | Atualizado em {{ $blog->updated_at->format('d/m/Y H:i:s') }}</small></p>
            <blockquote>{!! $blog->body !!}</blockquote>

            <hr>

            <a href="{{ route('blogs.index') }}" class="btn btn-xs btn-warning">Voltar</a>
            <a href="{{ route('blogs.edit', $blog->id) }}" class="btn btn-xs btn-primary">Editar</a>
            <a href="{{ route('blogs.destroy', $blog->id) }}" class="btn btn-xs btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form-blogs').submit();"><i class="fa fa-remove"></i> Deletar</a></li>
            <form id="delete-form-blogs" action="{{ route('blogs.destroy', $blog->id) }}" method="POST" style="display: none;">
             {!! csrf_field() !!}
             <input type="hidden" name="_method" value="DELETE">
           </form>

         </div><!-- panel-body -->


       </div><!-- panel-default -->
     </div><!-- col-md-12 -->


   </div><!-- row -->
 </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
@endsection

@section('jsPage')
@endsection