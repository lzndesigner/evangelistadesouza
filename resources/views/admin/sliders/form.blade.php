{!! csrf_field() !!}

<div class="row">
  <div class="col-md-2">
    <img src="/storage/{{ $slider->image ?? old('image') }}" alt="Sliders" class="img-thumbnail">    
  </div>
  <div class="col-md-10">
    <div class="form-group @if ($errors->has('image')) has-error @endif">
      <label for="image" class="form-label">Imagem do Slider</label>
      <input type="file" class="form-control" id="image" name="image" value="{{ $slider->image ?? old('image') }}">
      @if ($errors->has('image'))
      <span class="help-block">
        <strong>{{ $errors->first('image') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div>
</div>

<hr class="invisible">

<div class="form-group @if ($errors->has('title')) has-error @endif">
  <label for="title" class="form-label">Título do Slider</label>
  <input type="text" class="form-control" id="title" name="title" value="{{ $slider->title ?? old('title') }}" placeholder="Título do Slider" autofocus>
  @if ($errors->has('title'))
  <span class="help-block">
    <strong>{{ $errors->first('title') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group @if ($errors->has('url')) has-error @endif">
  <label for="url" class="form-label">Link do Slider</label>
  <input type="text" class="form-control" id="url" name="url" value="{{ $slider->url ?? old('url') }}" placeholder="URL do Slider" autofocus>
  @if ($errors->has('url'))
  <span class="help-block">
    <strong>{{ $errors->first('url') }}</strong>
  </span>
  @endif
  <span class="help-block">Caso não possui link, deixar em branco.</span>
</div><!-- form-group -->

<div class="row">
  <div class="col-md-6">
    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>Salvar</button>
    <a href="{{ route('sliders.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i>Cancelar</a>
  </div><!-- col-md-6 -->
</div>