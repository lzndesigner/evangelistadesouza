{!! csrf_field() !!}

<div class="row">
  <div class="col-md-2">
    <img src="/storage/{{ $client->image ?? old('image') }}" alt="Cliente" class="img-thumbnail">    
  </div>
  <div class="col-md-10">
    <div class="form-group @if ($errors->has('image')) has-error @endif">
      <label for="image" class="form-label">Imagem do Cliente</label>
      <input type="file" class="form-control" id="image" name="image" value="{{ $client->image ?? old('image') }}">
      @if ($errors->has('image'))
      <span class="help-block">
        <strong>{{ $errors->first('image') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div>
</div>

<hr class="invisible">

<div class="form-group @if ($errors->has('name')) has-error @endif">
  <label for="name" class="form-label">Nome do Cliente</label>
  <input type="text" class="form-control" id="name" name="name" value="{{ $client->name ?? old('name') }}" placeholder="Nome do Cliente" autofocus>
  @if ($errors->has('name'))
  <span class="help-block">
    <strong>{{ $errors->first('name') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group @if ($errors->has('link')) has-error @endif">
  <label for="link" class="form-label">Link do Cliente</label>
  <input type="text" class="form-control" id="link" name="link" value="{{ $client->link ?? old('link') }}" placeholder="URL do Cliente">
  @if ($errors->has('link'))
  <span class="help-block">
    <strong>{{ $errors->first('link') }}</strong>
  </span>
  @endif
</div><!-- form-group -->


<div class="form-group @if ($errors->has('status')) has-error @endif">
  <label for="status" class="form-label">Status do Cliente</label>
  <select class="form-control" id="status" name="status">
    @if(isset($client->status))
      @if($client->status == 0 )
        <option value="0" selected>Desabilitado</option> 
        <option value="1">Habilitar</option>
      @else
        <option value="0">Desabilitar</option> 
        <option value="1" selected>Habilitado</option>
      @endif
    @else
        <option value="1" selected>Habilitado</option>
        <option value="0">Desabilitado</option> 
    @endif
  </select>

  @if ($errors->has('status'))
  <span class="help-block">
    <strong>{{ $errors->first('status') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="row">
  <div class="col-md-6">
    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>Salvar</button>
    <a href="{{ route('clients.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i>Cancelar</a>
  </div><!-- col-md-6 -->
</div>