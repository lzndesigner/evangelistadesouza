@extends('admin.base')
@section('title', 'Detalhes do Cliente')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') - {{ $service->title }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('services.index') }}">Página de Clientes</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End service Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            <h2>{{ $service->title }}</h2>
          </div>

          <div class="panel-body">

            <img src="/storage/{{ $service->image ?? old('image') }}" alt="Clientes" class="img-thumbnail">

            <p><small>Criado em {{ $service->created_at->format('d/m/Y H:i:s') }} | Atualizado em {{ $service->updated_at->format('d/m/Y H:i:s') }}</small></p>
            <blockquote>{!! $service->body !!}</blockquote>

            <hr>

            <a href="{{ route('services.index') }}" class="btn btn-xs btn-warning">Voltar</a>
            <a href="{{ route('services.edit', $service->id) }}" class="btn btn-xs btn-primary">Editar</a>
            <a href="{{ route('services.destroy', $service->id) }}" class="btn btn-xs btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form-services').submit();"><i class="fa fa-remove"></i> Deletar</a></li>
            <form id="delete-form-services" action="{{ route('services.destroy', $service->id) }}" method="POST" style="display: none;">
             {!! csrf_field() !!}
             <input type="hidden" name="_method" value="DELETE">
           </form>

         </div><!-- panel-body -->


       </div><!-- panel-default -->
     </div><!-- col-md-12 -->


   </div><!-- row -->
 </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
@endsection

@section('jsPage')
@endsection