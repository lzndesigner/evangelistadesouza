<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="{{ $configs->config_description ?? '' }}">
  <meta name="keywords" content="{{ $configs->config_keywords ?? '' }}" />
  <title>{{ isset($configs->config_title) ? $configs->config_title : 'Evangelista de Souza' }} - Painel Administrativo</title>
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- ========== Css Files ========== -->
  <link href="{{ asset('backend/css/root.css?8') }}" rel="stylesheet">

  @yield('cssPage')

</head>
<body>
<div style="height:100%;">
  <!-- Start Page Loading -->
  <div class="loading"><img src="{{ asset('backend/img/loading.gif') }}" alt="loading-img"></div>
  <!-- End Page Loading -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->


  <!-- START TOP -->
  <div id="top" class="clearfix">

    <!-- Start App Logo -->
    <div class="applogo">
      <a href="{{ url('/') }}" class="logo"><img src="{{ asset('logo_branca.png') }}" alt="{{ isset($configs->config_title) ? $configs->config_title : 'Evangelista de Souza' }}"></a>
    </div>
    <!-- End App Logo -->

    <!-- Start Sidebar Show Hide Button -->
    <a href="#" class="sidebar-open-button"><i class="fa fa-bars"></i></a>
    <a href="#" class="sidebar-open-button-mobile"><i class="fa fa-bars"></i></a>
    <!-- End Sidebar Show Hide Button -->

    <!-- Start Sidepanel Show-Hide Button -->
  {{--<a href="#sidepanel" class="sidepanel-open-button"><i class="fa fa-outdent"></i></a>--}}
  <!-- End Sidepanel Show-Hide Button -->

    <!-- Start Top Right -->
    <ul class="top-right">
      <li class="dropdown link">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle profilebox"><img src="{{ asset('/galerias/avatares/10.png') }}" alt="{{ Auth::user()->name }}"><b>{{ Auth::user()->name }}</b><span class="caret"></span></a>
        <ul class="dropdown-menu dropdown-menu-list dropdown-menu-right">
          <li role="presentation" class="dropdown-header">Profile</li>
          <li class="divider"></li>
          <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa falist fa-power-off"></i> Logout</a></li>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </ul>
      </li>
    </ul>
    <!-- End Top Right -->
  </div>
  <!-- END TOP -->


  <!-- START SIDEBAR -->
@include('admin.includes.sidebar-left')
<!-- END SIDEBAR -->

  <!-- START CONTENT -->
  <div class="content">

  @yield('content')


  </div>
  <!-- End Content -->

  <!-- Start Footer -->
    <div class="row footer" style="bottom:0; z-index:2001;">
      <div class="col-md-6 text-left">
        {{ isset($configs->config_title) ? $configs->config_title : 'Meu site' }}  - Todos os Direitos Reservados {{ date('Y') }} ©
      </div>
      <div class="col-md-6 text-right">
        Desenvolvido por <a href="https://innsystem.com.br" target="_blank">InnSystem Inovação em Sistemas</a>
      </div>
    </div>
    <!-- End Footer -->

  <!-- START SIDEPANEL -->
@include('admin.includes.sidebar-right')
<!-- END SIDEPANEL -->
</div>

<script src="{{ mix('js/app.js') }}"></script>
<!-- //////////////////////////////////////////////////////////////////////////// -->
<!-- ================================================
jQuery Library
================================================ -->
<script type="text/javascript" src="{{ asset('backend/js/jquery.min.js') }}"></script>
<!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
<script src="{{ asset('backend/js/bootstrap/bootstrap.min.js') }}"></script>
<!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
<script type="text/javascript" src="{{ asset('backend/js/plugins.js') }}"></script>
<script type="text/javascript">
  $(function() {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      contentType: 'application/json'
    });
  })
</script>
<script src="{{ asset('js/jquery.maskedinput.js') }}"></script>
  <script>
    $(document).ready(function(){

      $('#config_phone').mask('(00) 0000-00009');
      $('#config_phone').blur(function(event) {
         if($(this).val().length == 14){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#config_phone').mask('(00) 0000-0000');
        } else {
          $('#config_phone').mask('(00) 90000-0000');
        }
      });
      
      $('#config_cellphone').mask('(00) 0000-00009');
      $('#config_cellphone').blur(function(event) {
         if($(this).val().length == 14){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#config_cellphone').mask('(00) 0000-0000');
        } else {
          $('#config_cellphone').mask('(00) 90000-0000');
        }
      });

    });
  </script>
@yield('jsPage')
</body>
</html>
