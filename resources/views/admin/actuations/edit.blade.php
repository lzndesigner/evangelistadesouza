@extends('admin.base')
@section('title', 'Editar Atuação')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') - {{ $actuation->title }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('actuations.index') }}">Página de Atuação</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End actuation Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
           @yield('title') - {{ $actuation->title }}
          </div>

          <div class="panel-body">

            <form action="{{ route('actuations.update', $actuation->id)}}" enctype="multipart/form-data" method="POST">
              <input type="hidden" name="_method" value="PUT">
             @include('admin.actuations.form')
           </form>

         </div><!-- panel-body -->


       </div><!-- panel-default -->
     </div><!-- col-md-12 -->


   </div><!-- row -->
 </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
@endsection

@section('jsPage')
<!-- ================================================
Bootstrap WYSIHTML5
================================================ -->
<!-- main file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js"></script>
<!-- bootstrap file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<!-- ================================================
Summernote
================================================ -->
<script type="text/javascript" src="/backend/js/summernote/summernote.min.js"></script>

<script>
/* BOOTSTRAP WYSIHTML5 */
$('.textarea').wysihtml5();

/* SUMMERNOTE*/
$(document).ready(function() {
  $('.summernote').summernote();
});
</script>

<!-- Query String ToSlug - Transforma o titulo em URL amigavel sem acentos ou espaço -->
<script type="text/javascript" src="/backend/js/jquery.stringToSlug.min.js"></script>
<script type="text/javascript">
$('input[name="title"]').stringToSlug({
  setEvents: 'keyup keydown blur',
  getPut: 'input[name="slug"]',
  space: '-',
  replace: '/\s?\([^\)]*\)/gi',
AND: 'e'
});
</script>
@endsection