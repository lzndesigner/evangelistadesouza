@extends('admin.base')
@section('title', 'Detalhes da Atuação')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') - {{ $actuation->title }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('actuations.index') }}">Página de Atuações</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End actuation Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            <h2>{{ $actuation->title }}</h2>
          </div>

          <div class="panel-body">

            <img src="/storage/{{ $actuation->image ?? old('image') }}" alt="Atuação" class="img-thumbnail" style="max-width:400px;">

            <p><small>Criado em {{ $actuation->created_at->format('d/m/Y H:i:s') }} | Atualizado em {{ $actuation->updated_at->format('d/m/Y H:i:s') }}</small></p>
            <blockquote>{!! $actuation->body !!}</blockquote>

            <hr>

            <a href="{{ route('actuations.index') }}" class="btn btn-xs btn-warning">Voltar</a>
            <a href="{{ route('actuations.edit', $actuation->id) }}" class="btn btn-xs btn-primary">Editar</a>
            <a href="{{ route('actuations.destroy', $actuation->id) }}" class="btn btn-xs btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form-actuations').submit();"><i class="fa fa-remove"></i> Deletar</a></li>
            <form id="delete-form-actuations" action="{{ route('actuations.destroy', $actuation->id) }}" method="POST" style="display: none;">
             {!! csrf_field() !!}
             <input type="hidden" name="_method" value="DELETE">
           </form>

         </div><!-- panel-body -->


       </div><!-- panel-default -->
     </div><!-- col-md-12 -->


   </div><!-- row -->
 </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
@endsection

@section('jsPage')
@endsection