{!! csrf_field() !!}

<div class="row">

  <div class="col-md-2">
    <img src="/storage/{{ $actuation->image ?? old('image') }}" alt="Atuação" class="img-thumbnail">    
  </div>
  <div class="col-md-10">
    <div class="form-group @if ($errors->has('image')) has-error @endif">
      <label for="image" class="form-label">Imagem da Página</label>
      <input type="file" class="form-control" id="image" name="image" value="{{ $actuation->image ?? old('image') }}">
      @if ($errors->has('image'))
      <span class="help-block">
        <strong>{{ $errors->first('image') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div>
</div>

<hr class="invisible">

<div class="form-group @if ($errors->has('title')) has-error @endif">
  <label for="title" class="form-label">Título da Atuação</label>
  <input type="text" class="form-control" id="title" name="title" value="{{ $actuation->title ?? old('title') }}" placeholder="Título da Atuação" autofocus>
  @if ($errors->has('title'))
  <span class="help-block">
    <strong>{{ $errors->first('title') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group @if ($errors->has('slug')) has-error @endif">
  <label for="slug" class="form-label">URL Amigável</label>
  <input type="text" class="form-control" id="slug" name="slug" readonly value="{{ $actuation->slug ?? old('slug') }}" placeholder="URL Amigável">
  <span class="help-block">* É preenchido automaticamente.</span>
  @if ($errors->has('slug'))
  <span class="help-block">
    <strong>{{ $errors->first('slug') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group @if ($errors->has('body')) has-error @endif">
  <label for="body" class="form-label">Conteúdo da Página</label>
  <textarea name="body" id="body" cols="30" rows="10" class="form-control textarea" placeholder="Conteúdo...">{{ $actuation->body ??  old('body') }}</textarea>
  @if ($errors->has('body'))
  <span class="help-block">
    <strong>{{ $errors->first('body') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="row">
  <div class="col-md-6">
    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>Salvar</button>
    <a href="{{ route('actuations.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i>Cancelar</a>
  </div><!-- col-md-6 -->
</div>