@extends('admin.base')
@section('title', 'Usuários Administrativos')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard/') }}">Inicio</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  @include('elements.messages')

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            Lista de @yield('title')
          </div>

            <div class="panel-body">
              <div class="table-responsive">
                 @if(count($users) > 0)
                <table class="table table-hover table-bordered table-striped" id="users-table">
                  <thead>
                    <tr>
                      <td width="6%">#</td>
                      <td>Nome</td>
                      <td>E-mail</td>
                      <td>Nível</td>
                      <td width="14%">Criação</td>
                      <td width="14%">Edição</td>
                      <td width="15%" class="text-right">Ações</td>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($users as $user)
                    <tr id="row-{{ $user->id }}">
                      <td>
                        <div class="checkbox">
                          <input class="users" id="checkbox{{ $user->id }}" data-id="{{ $user->id }}" type="checkbox">
                          <label for="checkbox{{ $user->id }}">
                            <b>{{ $user->id }}</b>
                          </label>
                        </div>
                      </td>
                      <td>{{ $user->name }}</td>
                      <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                      <td>
                        @if($user->level == 'admin')
                        <span class="label label-danger">Administrador</span>
                        @else
                        <span class="label label-primary">Moderador</span>
                        @endif
                      </td>
                      <td>{{ \Carbon\Carbon::parse($user->created_at)->format('d/m/y H:i')  }}</td>
                      <td>{{ \Carbon\Carbon::parse($user->updated_at)->format('d/m/y H:i') }}</td>
                      <td class="text-right">
                        <a href="{{ route('users.edit', $user->id) }}" class="btn btn-xs btn-primary btn-icon" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-xs btn-danger btn-icon btn-delete" data-id="{{ $user->id }}" data-toggle="tooltip" data-original-title="Remover"><i class="fa fa-remove"></i></a></li>

                      </td>
                    </tr>
                    @endforeach

                  </tbody>
                </table>
                @else
                    <div class="alert alert-info">Nenhum usuário administrativo cadastrado.</div>
                @endif

                <hr>

              </div><!-- table-responsive -->
            </div><!-- panel-body -->

            <div class="row">
              <div class="col-md-6">
                <a href="{{ route('users.create') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>Novo</a>
                <a href="#" class="btn btn-sm btn-danger btn-remove-all"><i class="fa fa-remove"></i>Remover selecionados</a>
              </div><!-- col-md-6 -->

              <div class="col-md-6 text-right">
                {{ $users->links() }}
              </div><!-- col-md-6 -->
            </div>


          </div><!-- panel-default -->
        </div><!-- col-md-12 -->


      </div><!-- row -->
    </div><!-- container-padding -->

  </div><!-- container-default -->
  <!-- END CONTAINER -->
  @endsection


  @section('cssPage')
  @endsection

  @section('jsPage')
  <script>
    $(function() {
      let btn = $('.btn-delete');
      let btnAll = $('.btn-remove-all');

      function remove(id) {
        let tr = $('#users-table #row-' + id);
        let request = $.ajax({
         url: '/dashboard/users/' + id,
         type: 'DELETE',
         statusCode: {
           204: function() {
               tr.fadeOut();
           },
           403: function() {
             let copy = tr.innerHTML;

             tr.html('').append('<td colspan="7">Esse usuario não foi removido.</td>')
                        .css({'background-color': 'red','color': 'white', 'text-align': 'center'});
           }
         }
        });
        return request;
      }

      btn.on('click', function(e) {
        e.preventDefault();
        let action = $(e.currentTarget);
        let request = remove(action.data('id'));
        request.fail(function() {
          window.alert('Não foi possivel remover o usaurio')
        });
      })

      btnAll.on('click', function(e) {
        e.preventDefault();

        let checkbox = $('input:checkbox[class="users"]:checked');

        if(checkbox.length > 0 && window.confirm('Gostaria de remover o(s) ' + checkbox.length + ' selecionado(s) usuário(s)?')) {
          //
          checkbox.each(function(index, el) {
            let id = $(el).data('id')
            let request = remove(id);
          })
        }

      })
    })
  </script>
  @endsection
