{!! csrf_field() !!}
<div class="form-group">
  <label for="name" class="form-label">Nome Completo</label>
  <input type="text" class="form-control" id="name" name="name" value="{{ $user->name ?? '' }}" placeholder="Nome Completo" autofocus>
  @if ($errors->has('name'))
  <span class="help-block">
    <strong>{{ $errors->first('name') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group">
  <label for="email" class="form-label">E-mail de Contato</label>
  <input type="email" class="form-control" id="email" name="email" value="{{ $user->email ?? '' }}" placeholder="E-mail de Contato">
  @if ($errors->has('email'))
  <span class="help-block">
    <strong>{{ $errors->first('email') }}</strong>
  </span>
  @endif
</div><!-- form-group -->


<div class="form-group @if ($errors->has('level')) has-error @endif">
  <label for="level" class="form-label">Nível de Acesso</label>
  <select class="form-control" id="level" name="level">
    @if(isset($user))
        @if($user->level == 'admin' )
          <option value="admin" selected>Administrador</option> 
          <option value="moder">Moderador</option>
        @else
          <option value="admin">Administrador</option> 
          <option value="moder" selected>Moderador</option>
        @endif
    @else
      <option disabled selected>-- Selecione o nível --</option> 
      <option value="admin">Administrador</option> 
      <option value="moder">Moderador</option>
    @endif
  </select>

  @if ($errors->has('level'))
  <span class="help-block">
    <strong>{{ $errors->first('level') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group">
  <label for="password" class="form-label">Senha</label>
  <input type="password" class="form-control" id="password" name="password" placeholder="******">
  @if ($errors->has('password'))
  <span class="help-block">
    <strong>{{ $errors->first('password') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group">
  <label for="password" class="form-label">Confirmar Senha</label>
  <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="******">
</div><!-- form-group -->

<div class="row">
  <div class="col-md-6">
    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>Salvar</button>
    <a href="{{ route('users.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i>Cancelar</a>
  </div><!-- col-md-6 -->
</div>