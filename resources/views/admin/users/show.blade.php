@extends('admin.base')
@section('title', 'Detalhes do Usuário Administrativo')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') - {{ $user->name }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('users.index') }}">Usuários Administrativos</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">

            <div class="widget profile-widget" style="margin:0; height:380px; background:url(/galerias/fundos/socialbg.jpg) center center;">
              <img src="{{ asset('/galerias/avatares/10.png') }}" class="profile-image" alt="{{ $user->name }}">
              <h1>{{ $user->name }}</h1>
              <ul class="stats widget-inline-list clearfix">
                <li class="col-md-5"><span><i class="fa fa-envelope"></i> E-mail</span>{{ $user->email }}</li>
                <li class="col-md-4"><span><i class="fa fa-map-marker"></i> Cidade</span>Ribeirão Preto/SP</li>
                <li class="col-md-3"><span><i class="fa fa-plus"></i> Nível</span>Administrador</li>
              </ul>
            </div>

            <hr>

            <a href="{{ route('users.index') }}" class="btn btn-xs btn-warning">Voltar</a>
            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-xs btn-primary">Editar</a>
            <a href="{{ route('users.destroy', $user->id) }}" class="btn btn-xs btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form-users').submit();"><i class="fa fa-remove"></i> Deletar</a></li>
            <form id="delete-form-users" action="{{ route('users.destroy', $user->id) }}" method="POST" style="display: none;">
             {!! csrf_field() !!}
             <input type="hidden" name="_method" value="DELETE">
           </form>

         </div><!-- panel-body -->


       </div><!-- panel-default -->
     </div><!-- col-md-12 -->


   </div><!-- row -->
 </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
@endsection

@section('jsPage')
@endsection