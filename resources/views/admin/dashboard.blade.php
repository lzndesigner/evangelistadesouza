@extends('admin.base')
@section('title', 'Painel de Controle')

@section('content')
  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">@yield('title')</h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Inicio</a></li>
        <li class="active">@yield('title')</li>
      </ol>
  </div>
  <!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">Seja bem vindo!</div>

                <div class="panel-body">
                    Você está no seu <b>Painel Administrativo</b>, nessa área você terá acesso aos usuários administrativo cadastrados, toda a configuração geral do site e muitos outros recursos nesse completo Gerenciador de Conteúdo.
                </div>
            </div>
        </div>
    </div>
</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection
