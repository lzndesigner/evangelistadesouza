@extends('admin.base')
@section('title', 'Registrar Reserva')

@section('content')
  <!-- Start Page Header -->
  <div class="page-header">
    <h1 class="title">@yield('title')</h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/dashboard/') }}">Inicio</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
  <!-- End Page Header -->

  <!-- START CONTAINER -->
  <div class="container-default" id="app">
    @include('elements.messages')
    <div class="container-padding">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-title">
              @yield('title')
            </div>
            <div class="panel-body">
              <form action="{{ route('reservations.insert')}}" enctype="multipart/form-data" method="POST">
                @include('admin.reservations.forms.new')
              </form>
            </div><!-- panel-body -->
          </div><!-- panel-default -->
        </div><!-- col-md-12 -->
      </div><!-- row -->
    </div><!-- container-padding -->
  </div><!-- container-default -->
  <!-- END CONTAINER -->
@endsection