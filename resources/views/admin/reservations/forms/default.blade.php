<div id="app">
  {!! csrf_field() !!}

  @if($preRegistration !== null)
    <h4>Dados Pessoais</h4>
    <div class="row">
      <div class="col-md-3">
        <div class="form-group @if ($errors->has('name')) has-error @endif">
          <label for="name" class="form-label">Nome Completo</label>
          <input type="text" class="form-control" id="name" value="{{ $preRegistration->name }}" placeholder="Nome Completo" autofocus readonly>
        </div><!-- form-group -->
      </div><!-- col-md-3 -->

      <div class="col-md-3">
        <div class="form-group @if ($errors->has('email')) has-error @endif">
          <label for="title" class="form-label">E-mail de Contato</label>
          <input type="email" class="form-control" id="email" value="{{ $preRegistration->email }}" placeholder="E-mail de Contato" readonly>
        </div><!-- form-group -->
      </div><!-- col-md-3 -->

      <div class="col-md-3">
        <div class="form-group @if ($errors->has('phone')) has-error @endif">
          <label for="phone" class="form-label">Telefone</label>
          <input type="text" class="form-control" id="phone" value="{{ $preRegistration->phone }}" placeholder="(16) 99274-7526" readonly>
        </div><!-- form-group -->
      </div><!-- col-md-3 -->


      <div class="col-md-3">
        <div class="form-group @if ($errors->has('city')) has-error @endif">
          <label for="city" class="form-label">Cidade</label>
          <div class="input-group">
            <input type="text" class="form-control" id="city" value="{{ $preRegistration->city }}" placeholder="Cidade" readonly>
            <div class="input-group-addon">UF</div>
          </div>
        </div><!-- form-group -->
      </div><!-- col-md-3 -->

    </div> <!-- row -->
    @endif

  <hr class="clearfix">

  <h4>Dados da Reserva</h4>
  <div class="row">

    <div class="col-md-8">
      <div class="form-group @if ($errors->has('propietario')) has-error @endif">
        <label for="propietario" class="form-label">Proprietário</label>
        <input type="text" class="form-control" id="propietario" name="propietario" value="{{ $reservation->propietario ?? old('propietario') }}" placeholder="Proprietário">
        @if ($errors->has('propietario'))
          <span class="help-block">
        <strong>{{ $errors->first('propietario') }}</strong>
      </span>
        @endif
      </div><!-- form-group -->
    </div><!-- col-md-8 -->

    <div class="col-md-4">
      <div class="form-group @if ($errors->has('fazenda')) has-error @endif">
        <label for="fazenda" class="form-label">Fazenda</label>
        <input type="text" class="form-control" id="fazenda" name="fazenda" value="{{ $reservation->fazenda ?? old('fazenda') }}" placeholder="Fazenda">
        @if ($errors->has('fazenda'))
          <span class="help-block">
        <strong>{{ $errors->first('fazenda') }}</strong>
      </span>
        @endif
      </div><!-- form-group -->
    </div><!-- col-md-4 -->

    <div class="clearfix bottomargin-sm"></div>

    <div class="col-md-3">
      <div class="form-group @if ($errors->has('matricula')) has-error @endif">
        <label for="matricula" class="form-label">Matrícula</label>
        <input type="text" class="form-control" id="matricula" name="matricula" value="{{ $reservation->matricula ?? old('matricula') }}" placeholder="Matrícula">
        @if ($errors->has('matricula'))
          <span class="help-block">
        <strong>{{ $errors->first('matricula') }}</strong>
      </span>
        @endif
      </div><!-- form-group -->
    </div><!-- col-md-3 -->

    <div class="col-md-3">
      <div class="form-group @if ($errors->has('municipio_cartorio')) has-error @endif">
        <label for="municipio_cartorio" class="form-label">Município do Cartório</label>
        <input type="text" class="form-control" id="municipio_cartorio" name="municipio_cartorio" value="{{ $reservation->municipio_cartorio ?? old('municipio_cartorio') }}" placeholder="Município do Cartório">
        @if ($errors->has('municipio_cartorio'))
          <span class="help-block">
        <strong>{{ $errors->first('municipio_cartorio') }}</strong>
      </span>
        @endif
      </div><!-- form-group -->
    </div><!-- col-md-3 -->

    <div class="col-md-3">
      <div class="form-group @if ($errors->has('municipio')) has-error @endif">
        <label for="municipio" class="form-label">Município</label>
        <input type="text" class="form-control" id="municipio" name="municipio" value="{{ $reservation->municipio ?? $preRegistration->city ?? old('municipio') }}" placeholder="Município">
        @if ($errors->has('municipio'))
          <span class="help-block">
        <strong>{{ $errors->first('municipio') }}</strong>
      </span>
        @endif
      </div><!-- form-group -->
    </div><!-- col-md-3 -->

    <div class="col-md-3">
      <div class="form-group @if ($errors->has('estado')) has-error @endif">
        <label for="estado" class="form-label">Estado</label>
        <input type="text" class="form-control" id="estado" name="estado" value="{{ $reservation->estado ?? $preRegistration->state ?? null }}" placeholder="Estado">
        @if ($errors->has('estado'))
          <span class="help-block">
        <strong>{{ $errors->first('estado') }}</strong>
      </span>
        @endif
      </div><!-- form-group -->
    </div><!-- col-md-3 -->

    <div class="clearfix bottomargin-sm"></div>

    <div class="col-md-2">
      <div class="form-group @if ($errors->has('bioma')) has-error @endif">
        <label for="bioma" class="form-label">Bioma</label>
        <select class="form-control" id="bioma" name="bioma">
          <option value="Cerrado" {{ $reservation && $reservation->bioma === 'Cerrado' ? 'selected' : '' }}>Cerrado</option>
          <option value="Caatinga" {{ $reservation && $reservation->bioma === 'Caatinga' ? 'selected' : '' }}>Caatinga</option>
          <option value="Amazônia" {{ $reservation && $reservation->bioma === 'Amazônia' ? 'selected' : '' }}>Amazônia</option>
          <option value="Mata Atlântica" {{ $reservation && $reservation->bioma === 'Mata Atlântica' ? 'selected' : '' }}>Mata Atlântica</option>
          <option value="Pantanal" {{ $reservation && $reservation->bioma === 'Pantanal' ? 'selected' : '' }}>Pantanal</option>
          <option value="Pampa" {{ $reservation && $reservation->bioma === 'Pampa' ? 'selected' : '' }}>Pampa</option>
        </select>
        @if ($errors->has('bioma'))
          <span class="help-block">
        <strong>{{ $errors->first('bioma') }}</strong>
      </span>
        @endif
      </div><!-- form-group -->
    </div><!-- col-md-2 -->

    <div class="col-md-2">
      <div class="form-group @if ($errors->has('situacao')) has-error @endif">
        <label for="situacao" class="form-label">Situação do Imóvel</label>
        <select class="form-control" id="situacao" name="situacao">
          <option value="regular" {{ $reservation && $reservation->situacao === 'regular' ? 'selected' : '' }}>Regular</option>
          <option value="irregular" {{ $reservation && $reservation->situacao === 'irregular' ? 'selected' : '' }}>Irregular</option>
        </select>
        @if ($errors->has('situacao'))
          <span class="help-block">
        <strong>{{ $errors->first('situacao') }}</strong>
      </span>
        @endif
      </div><!-- form-group -->
    </div><!-- col-md-2 -->

    <div class="col-md-2">
      <div class="form-group @if ($errors->has('georeferencia')) has-error @endif">
        <label for="georeferencia" class="form-label">Área Georeferenciada</label>
        <select class="form-control" id="georeferencia" name="georeferencia">
          <option value="sim" {{ $reservation && $reservation->georeferencia === 'sim' ? 'selected' : '' }}>Sim</option>
          <option value="nao" {{ $reservation && $reservation->georeferencia === 'nao' ? 'selected' : '' }}>Não</option>
        </select>
        @if ($errors->has('georeferencia'))
          <span class="help-block">
        <strong>{{ $errors->first('georeferencia') }}</strong>
      </span>
        @endif
      </div><!-- form-group -->
    </div><!-- col-md-2 -->

    <div class="col-md-2">
      <div class="form-group @if ($errors->has('inscricao_car')) has-error @endif">
        <label for="inscricao_car" class="form-label">Possui Inscrição CAR</label>
        <input type="text" class="form-control" id="inscricao_car" name="inscricao_car" value="{{ $reservation->inscricao_car ?? old('inscricao_car') }}" placeholder="preencha caso possua">
        @if ($errors->has('inscricao_car'))
          <span class="help-block">
        <strong>{{ $errors->first('inscricao_car') }}</strong>
      </span>
        @endif
      </div><!-- form-group -->
    </div><!-- col-md-2 -->

    <div class="col-md-4">
      <div class="form-group @if ($errors->has('tamanho_area')) has-error @endif">
        <label for="tamanho_area" class="form-label">Tamanho da Área</label>
        <div class="row">
          <div class="col-md-7">
            <input type="text" class="form-control" id="tamanho_area" name="tamanho_area" value="{{ $reservation->tamanho_area ?? old('tamanho_area') }}" placeholder="120">
          </div>

          <div class="col-md-5">
            <select class="form-control" id="area_tipo" name="area_tipo">
              <option value="hectares" {{ $reservation && $reservation->area_tipo === 'hectares' ? 'selected' : '' }}>Hectares</option>
              <option value="alqueires" {{ $reservation && $reservation->area_tipo === 'alqueires' ? 'selected' : '' }}>Alqueires</option>
            </select>
          </div>

        </div>

        @if ($errors->has('tamanho_area'))
          <span class="help-block">
        <strong>{{ $errors->first('tamanho_area') }}</strong>
      </span>
        @endif

        @if ($errors->has('area_tipo'))
          <span class="help-block">
        <strong>{{ $errors->first('area_tipo') }}</strong>
      </span>
        @endif
      </div><!-- form-group -->
    </div><!-- col-md-2 -->

  </div><!-- row -->

  <hr class="clearfix">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group @if ($errors->has('descricao')) has-error @endif">
        <label for="descricao" class="form-label">Descrição da Negóciação</label>
        <textarea name="descricao"
                  id="descricao"
                  cols="30"
                  rows="10"
                  class="form-control textarea"
                  placeholder="Faça uma breve descrição...">{{ $reservation->descricao ?? old('descricao') }}</textarea>
        @if ($errors->has('descricao'))
          <span class="help-block">
    <strong>{{ $errors->first('descricao') }}</strong>
  </span>
        @endif
      </div><!-- form-group -->
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>Salvar</button>
      <a href="{{ route('reservations.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i>Cancelar</a>
    </div><!-- col-md-6 -->
  </div>
</div>