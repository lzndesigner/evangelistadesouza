<div class="sidebar clearfix">

    <ul class="sidebar-panel nav">
        <li class="sidetitle">NAVEGAÇÃO</li>
        <li><a href="{{ url('/') }}" target="_Blank"><span class="icon color5"><i class="fa fa-desktop"></i></span>Meu Site</a></li>
        <li><a href="{{ url('/dashboard') }}"><span class="icon color5"><i class="fa fa-home"></i></span>Início</a></li>
	<li><a href="http://webmail.innsystem.com.br" target="_Blank"><span class="icon color5"><i class="fa fa-comments"></i></span>Web Mail</a></li>

        <li><a href="#"><span class="icon color7"><i class="fa fa-cogs"></i></span> Configuração <span class="caret"></span></a>
            <ul>
                <li><a href="{{ route('configs.index') }}"><i class="fa fa-cog"></i>Site em Geral</a></li>
                <li><a href="{{ route('users.index') }}"><i class="fa fa-user"></i>Usuários Administrativos</a></li>
            </ul>
        </li>
   

        <li><a href="#"><span class="icon color7"><i class="fa fa-book"></i></span> Páginas <span class="caret"></span></a>
            <ul>
                <li><a href="{{ route('office.index') }}"><i class="fa fa-bank"></i> O Escritório</a></li>
                <li><a href="{{ url('/dashboard/office-photos') }}"><i class="fa fa-camera"></i> Galeria de Fotos</a></li>
                <li><a href="{{ route('actuations.index') }}"><i class="fa fa-cog"></i> Atuação</a></li>
                <li><a href="{{ route('professionals.index') }}"><i class="fa fa-users"></i> Profissional</a></li>
                <li><a href="{{ route('blogs.index') }}"><i class="fa fa-comments"></i> Notícias</a></li>
                <li><a href="{{ route('reviews.index') }}"><i class="fa fa-heart"></i> Depoimentos</a></li>
                <li><a href="{{ route('sliders.index') }}"><i class="fa fa-image"></i> Sliders</a></li>
                @if(Auth::User()->level === 'developer')
                <li><a href="{{ route('pages.index') }}"><i class="fa fa-book"></i> Página de Informações</a></li>
                @endif
            </ul>
        </li>
    </ul>

    <ul class="sidebar-panel nav">
        <li class="sidetitle">OUTROS</li>
        <li><a href="{{ url('https://www.innsystem.com.br/central-ajuda') }}" target="_Blank"><span class="icon color15"><i class="fa fa-bank"></i></span>Central de Ajuda</a></li>
        <li><a href="{{ url('https://www.innsystem.com.br/contato') }}" target="_Blank"><span class="icon color15"><i class="fa fa-tags"></i></span>Ticket de Suporte</a></li>
    </ul>

</div><!-- sidebar -->
