@extends('admin.base')
@section('title', 'Detalhes do Profissional')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') - {{ $professional->title }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('professionals.index') }}">Página dos Profissionais</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End professional Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            <h2>{{ $professional->title }}</h2>
          </div>

          <div class="panel-body">

            <img src="/storage/{{ $professional->image ?? old('image') }}" alt="Profissionais" class="img-thumbnail">

            <p><small>Criado em {{ $professional->created_at->format('d/m/Y H:i:s') }} | Atualizado em {{ $professional->updated_at->format('d/m/Y H:i:s') }}</small></p>
            <blockquote>{!! $professional->body !!}</blockquote>

            <hr>

            <a href="{{ route('professionals.index') }}" class="btn btn-xs btn-warning">Voltar</a>
            <a href="{{ route('professionals.edit', $professional->id) }}" class="btn btn-xs btn-primary">Editar</a>
            <a href="{{ route('professionals.destroy', $professional->id) }}" class="btn btn-xs btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form-professionals').submit();"><i class="fa fa-remove"></i> Deletar</a></li>
            <form id="delete-form-professionals" action="{{ route('professionals.destroy', $professional->id) }}" method="POST" style="display: none;">
             {!! csrf_field() !!}
             <input type="hidden" name="_method" value="DELETE">
           </form>

         </div><!-- panel-body -->


       </div><!-- panel-default -->
     </div><!-- col-md-12 -->


   </div><!-- row -->
 </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
@endsection

@section('jsPage')
@endsection