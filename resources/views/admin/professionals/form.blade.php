{!! csrf_field() !!}

<div class="row">
  <div class="col-md-2">
    <img src="/storage/{{ $professional->image ?? old('image') }}" alt="Profissional" class="img-thumbnail">    
  </div>
  <div class="col-md-10">
    <div class="form-group @if ($errors->has('image')) has-error @endif">
      <label for="image" class="form-label">Imagem do Profissional</label>
      <input type="file" class="form-control" id="image" name="image" value="{{ $professional->image ?? old('image') }}">
      @if ($errors->has('image'))
      <span class="help-block">
        <strong>{{ $errors->first('image') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div>
</div>

<hr class="invisible">

<div class="form-group @if ($errors->has('name')) has-error @endif">
  <label for="name" class="form-label">Nome do Profissional</label>
  <input type="text" class="form-control" id="name" name="name" value="{{ $professional->name ?? old('name') }}" placeholder="Nome do Profissional" autofocus>
  @if ($errors->has('name'))
  <span class="help-block">
    <strong>{{ $errors->first('name') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group @if ($errors->has('oab')) has-error @endif">
  <label for="oab" class="form-label">OAB do Profissional</label>
  <input type="text" class="form-control" id="oab" name="oab" value="{{ $professional->oab ?? old('oab') }}" placeholder="OAB do Profissional">
  @if ($errors->has('oab'))
  <span class="help-block">
    <strong>{{ $errors->first('oab') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group @if ($errors->has('body')) has-error @endif">
  <label for="body" class="form-label">Descrição do Profissional</label>
  <textarea rows="18" class="form-control textarea" id="body" name="body" value="{{ $professional->body ?? old('body') }}" placeholder="Descrição do Profissional" autofocus>{{ $professional->body ?? old('body') }}</textarea>
  @if ($errors->has('body'))
  <span class="help-block">
    <strong>{{ $errors->first('body') }}</strong>
  </span>
  @endif
</div><!-- form-group -->



<div class="form-group @if ($errors->has('status')) has-error @endif">
  <label for="status" class="form-label">Status do Profissional</label>
  <select class="form-control" id="status" name="status">
    @if(isset($professional->status))
    @if($professional->status == 0 )
    <option value="0" selected>Desabilitado</option> 
    <option value="1">Habilitar</option>
    @else
    <option value="0">Desabilitar</option> 
    <option value="1" selected>Habilitado</option>
    @endif
    @else
    <option value="1" selected>Habilitado</option>
    <option value="0">Desabilitado</option> 
    @endif
  </select>

  @if ($errors->has('status'))
  <span class="help-block">
    <strong>{{ $errors->first('status') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="row">
  <div class="col-md-6">
    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>Salvar</button>
    <a href="{{ route('clients.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i>Cancelar</a>
  </div><!-- col-md-6 -->
</div>