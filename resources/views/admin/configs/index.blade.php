@extends('admin.base')
@section('title', 'Configuração Geral')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard/') }}">Inicio</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  @include('elements.messages')

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            @yield('title')
          </div>

          <div class="panel-body">
            <div class="table-responsive">

              <form action="{{ route('configs.store')}}" method="POST">
                {!! csrf_field() !!}

                <div role="tabpanel">

                  <!-- Nav tabs -->
                  <ul class="nav nav-pills" role="tablist">
                    <li role="presentation" class="active"><a href="#config_geral" aria-controls="config_geral" role="tab" data-toggle="tab">Geral</a></li>
                    <li role="presentation"><a href="#config_site" aria-controls="config_site" role="tab" data-toggle="tab">Site</a></li>
                    <li role="presentation"><a href="#config_localizacao" aria-controls="config_localizacao" role="tab" data-toggle="tab">Localização</a></li>
                    <li role="presentation"><a href="#config_redesocial" aria_controls="config_redesocial" role="tab" data-toggle="tab">Rede Sociais</a></li>
                  </ul>

                  <!-- Tab panes -->
                  <div class="tab-content">

                    <div role="tabpanel" class="tab-pane active" id="config_geral">
                      <!-- conteudo -->

                      <div class="form-group @if ($errors->has('config_title')) has-error @endif">
                        <label for="config_title" class="form-label">Título do Site</label>
                        <input type="text" class="form-control" id="config_title" name="config_title" value="{{ $config->config_title ?? old('config_title') }}" placeholder="Título do Site" autofocus>
                        @if ($errors->has('config_title'))
                        <span class="help-block">
                          <strong>{{ $errors->first('config_title') }}</strong>
                        </span>
                        @endif
                      </div><!-- form-group -->

                      <div class="form-group @if ($errors->has('config_description')) has-error @endif">
                        <label for="config_description" class="form-label">Descrição do Site</label>
                        <input type="text" class="form-control" id="config_description" name="config_description" value="{{ $config->config_description ?? old('config_description') }}" placeholder="Descrição do Site">
                        @if ($errors->has('config_description'))
                        <span class="help-block">
                          <strong>{{ $errors->first('config_description') }}</strong>
                        </span>
                        @endif
                      </div><!-- form-group -->

                      <div class="form-group @if ($errors->has('config_keywords')) has-error @endif">
                        <label for="config_keywords" class="form-label">Palavras Chaves</label>
                        <input type="text" class="form-control" id="config_keywords" name="config_keywords" value="{{ $config->config_keywords ?? old('config_keywords') }}" placeholder="Palavras Chaves">
                        @if ($errors->has('config_keywords'))
                        <span class="help-block">
                          <strong>{{ $errors->first('config_keywords') }}</strong>
                        </span>
                        @endif
                      </div><!-- form-group -->

                      <!-- conteudo -->
                    </div><!-- tabpanel -->

                    <div role="tabpanel" class="tab-pane" id="config_site">
                      <!-- conteudo -->

                      <div class="form-group @if ($errors->has('company_name')) has-error @endif">
                        <label for="company_name" class="form-label">Nome do Site</label>
                        <input type="text" class="form-control" id="company_name" name="company_name" value="{{ $config->company_name ?? old('company_name') }}" placeholder="Nome do Site">
                        @if ($errors->has('company_name'))
                        <span class="help-block">
                          <strong>{{ $errors->first('company_name') }}</strong>
                        </span>
                        @endif
                      </div><!-- form-group -->

                      <div class="form-group @if ($errors->has('company_proprietary')) has-error @endif">
                        <label for="company_proprietary" class="form-label">Proprietário do Site</label>
                        <input type="text" class="form-control" id="company_proprietary" name="company_proprietary" value="{{ $config->company_proprietary ?? old('company_proprietary') }}" placeholder="Proprietário do Site">
                        @if ($errors->has('company_proprietary'))
                        <span class="help-block">
                          <strong>{{ $errors->first('company_proprietary') }}</strong>
                        </span>
                        @endif
                      </div><!-- form-group -->

                      <div class="form-group @if ($errors->has('config_email')) has-error @endif">
                        <label for="config_email" class="form-label">E-mail de Contato</label>
                        <input type="text" class="form-control" id="config_email" name="config_email" value="{{ $config->config_email ?? old('config_email') }}" placeholder="E-mail de Contato">
                        @if ($errors->has('config_email'))
                        <span class="help-block">
                          <strong>{{ $errors->first('config_email') }}</strong>
                        </span>
                        @endif
                      </div><!-- form-group -->

                      <div class="form-group @if ($errors->has('config_phone')) has-error @endif">
                        <label for="config_phone" class="form-label">Telefone de Contato</label>
                        <input type="text" class="form-control" id="config_phone" name="config_phone" value="{{ $config->config_phone ?? old('config_phone') }}" placeholder="Telefone de Contato">
                        @if ($errors->has('config_phone'))
                        <span class="help-block">
                          <strong>{{ $errors->first('config_phone') }}</strong>
                        </span>
                        @endif
                      </div><!-- form-group -->

                      <div class="form-group @if ($errors->has('config_cellphone')) has-error @endif">
                        <label for="config_cellphone" class="form-label">Celular de Contato</label>
                        <input type="text" class="form-control" id="config_cellphone" name="config_cellphone" value="{{ $config->config_cellphone ?? old('config_cellphone') }}" placeholder="Celular de Contato">
                        @if ($errors->has('config_cellphone'))
                        <span class="help-block">
                          <strong>{{ $errors->first('config_cellphone') }}</strong>
                        </span>
                        @endif
                      </div><!-- form-group -->

                      <div class="form-group @if ($errors->has('config_information')) has-error @endif">
                        <label for="config_information" class="form-label">Informações Adicionais</label>
                        <input type="text" class="form-control" id="config_information" name="config_information" value="{{ $config->config_information ?? old('config_information') }}" placeholder="Informações de Contato">
                        @if ($errors->has('config_information'))
                        <span class="help-block">
                          <strong>{{ $errors->first('config_information') }}</strong>
                        </span>
                        @endif
                      </div><!-- form-group -->


                      <!-- conteudo -->
                    </div><!-- tabpanel -->

                    <div role="tabpanel" class="tab-pane" id="config_localizacao">
                      <!-- conteudo -->

                      <div class="form-group @if ($errors->has('company_address')) has-error @endif">
                        <label for="company_address" class="form-label">Endereço</label>
                        <input type="text" class="form-control" id="company_address" name="company_address" value="{{ $config->company_address ?? old('company_address') }}" placeholder="Endereço">
                        @if ($errors->has('company_address'))
                        <span class="help-block">
                          <strong>{{ $errors->first('company_address') }}</strong>
                        </span>
                        @endif
                      </div><!-- form-group -->

                      <div class="form-group @if ($errors->has('company_city')) has-error @endif">
                        <label for="company_city" class="form-label">Cidade</label>
                        <input type="text" class="form-control" id="company_city" name="company_city" value="{{ $config->company_city ?? old('company_city') }}" placeholder="Cidade">
                        @if ($errors->has('company_city'))
                        <span class="help-block">
                          <strong>{{ $errors->first('company_city') }}</strong>
                        </span>
                        @endif
                      </div><!-- form-group -->

                      <div class="form-group @if ($errors->has('company_state')) has-error @endif">
                        <label for="company_state" class="form-label">Estado</label>
                        <input type="text" class="form-control" id="company_state" name="company_state" value="{{ $config->company_state ?? old('company_state') }}" placeholder="Estado">
                        @if ($errors->has('company_state'))
                        <span class="help-block">
                          <strong>{{ $errors->first('company_state') }}</strong>
                        </span>
                        @endif
                      </div><!-- form-group -->

                      <div class="form-group @if ($errors->has('company_country')) has-error @endif">
                        <label for="company_country" class="form-label">País</label>
                        <select name="company_country" id="company_country" class="form-control">
                          <option value="" disabled>Selecione seu País</option>
                          <option value="{{ $config->company_country ?? old('company_country') }}" selected>{{ $config->company_country ?? old('company_country') }}</option>
                        </select>
                        @if ($errors->has('company_country'))
                        <span class="help-block">
                          <strong>{{ $errors->first('company_country') }}</strong>
                        </span>
                        @endif
                      </div><!-- form-group -->

                      <!-- conteudo -->
                    </div><!-- tabpanel -->     

                    <div role="tabpanel" class="tab-pane" id="config_redesocial">
                      <!-- conteudo -->

                      <div class="form-group @if ($errors->has('redesocial_facebook')) has-error @endif">
                        <label for="redesocial_facebook" class="form-label">Facebook</label>
                        <input type="text" class="form-control" id="redesocial_facebook" name="redesocial_facebook" value="{{ $config->redesocial_facebook ?? old('redesocial_facebook') }}" placeholder="Facebook" autofocus>
                        <span class="help-block">Colocar link completo <i>https://facebook.com/</i><b>sua_página</b></span>
                        @if ($errors->has('redesocial_facebook'))
                        <span class="help-block">
                          <strong>{{ $errors->first('redesocial_facebook') }}</strong>
                        </span>
                        @endif
                      </div><!-- form-group -->

                      <div class="form-group @if ($errors->has('redesocial_instagram')) has-error @endif">
                        <label for="redesocial_instagram" class="form-label">Instagram</label>
                        <input type="text" class="form-control" id="redesocial_instagram" name="redesocial_instagram" value="{{ $config->redesocial_instagram ?? old('redesocial_instagram') }}" placeholder="Instagram" autofocus>
                        <span class="help-block">Colocar apenas <b>@página</b></span>
                        @if ($errors->has('redesocial_instagram'))
                        <span class="help-block">
                          <strong>{{ $errors->first('redesocial_instagram') }}</strong>
                        </span>
                        @endif
                      </div><!-- form-group -->
                      <div class="form-group @if ($errors->has('redesocial_twitter')) has-error @endif">
                        <label for="redesocial_twitter" class="form-label">Twitter</label>
                        <input type="text" class="form-control" id="redesocial_twitter" name="redesocial_twitter" value="{{ $config->redesocial_twitter ?? old('redesocial_twitter') }}" placeholder="Twitter" autofocus>
                        <span class="help-block">Colocar apenas <b>@página</b></span>
                        @if ($errors->has('redesocial_twitter'))
                        <span class="help-block">
                          <strong>{{ $errors->first('redesocial_twitter') }}</strong>
                        </span>
                        @endif
                      </div><!-- form-group -->


                      <!-- conteudo -->
                    </div><!-- tabpanel -->     

                  </div><!-- tab-content -->

                </div><!-- tabpanel -->

                <hr>
                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>Salvar</button>
                

              </form>

            </div><!-- table-responsive -->
          </div><!-- panel-body -->


          

        </div><!-- panel-default -->
      </div><!-- col-md-12 -->
      

    </div><!-- row -->
  </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection


@section('cssPage')
@endsection

@section('jsPage')
@endsection
