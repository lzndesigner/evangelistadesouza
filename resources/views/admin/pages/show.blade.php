@extends('admin.base')
@section('title', 'Detalhes da Página')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') - {{ $page->title }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('pages.index') }}">Página de Informação</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            {{ $page->title }}
          </div>

          <div class="panel-body">

            <p><small>Criado em {{ $page->created_at->format('d/m/Y H:i:s') }} | Atualizado em {{ $page->updated_at->format('d/m/Y H:i:s') }}</small></p>
            <blockquote>{!! $page->body !!}</blockquote>

            <hr>

            <a href="{{ route('pages.index') }}" class="btn btn-xs btn-warning">Voltar</a>
            <a href="{{ route('pages.edit', $page->id) }}" class="btn btn-xs btn-primary">Editar</a>
            <a href="{{ route('pages.destroy', $page->id) }}" class="btn btn-xs btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form-pages').submit();"><i class="fa fa-remove"></i> Deletar</a></li>
            <form id="delete-form-pages" action="{{ route('pages.destroy', $page->id) }}" method="POST" style="display: none;">
             {!! csrf_field() !!}
             <input type="hidden" name="_method" value="DELETE">
           </form>

         </div><!-- panel-body -->


       </div><!-- panel-default -->
     </div><!-- col-md-12 -->


   </div><!-- row -->
 </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
@endsection

@section('jsPage')
@endsection