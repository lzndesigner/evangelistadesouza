{!! csrf_field() !!}

<div class="form-group @if ($errors->has('name')) has-error @endif">
  <label for="name" class="form-label">Nome do Cliente</label>
  <input type="text" class="form-control" id="name" name="name" value="{{ $review->name ?? old('name') }}" placeholder="Nome do Cliente" autofocus>
  @if ($errors->has('name'))
  <span class="help-block">
    <strong>{{ $errors->first('name') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group @if ($errors->has('charge')) has-error @endif">
  <label for="charge" class="form-label">Cargo do Cliente</label>
  <input type="text" class="form-control" id="charge" name="charge" value="{{ $review->charge ?? old('charge') }}" placeholder="Cargo do Cliente">
  @if ($errors->has('charge'))
  <span class="help-block">
    <strong>{{ $errors->first('charge') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group @if ($errors->has('body')) has-error @endif">
  <label for="body" class="form-label">Depoimento do Cliente</label>
  <textarea class="form-control" rows="4" id="body" name="body" value="{{ $review->body ?? old('body') }}" placeholder="Depoimento do Cliente">{{ $review->body ?? old('body') }}</textarea>
  @if ($errors->has('body'))
  <span class="help-block">
    <strong>{{ $errors->first('body') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group @if ($errors->has('status')) has-error @endif">
  <label for="status" class="form-label">Status do Depoimento</label>
  <select class="form-control" id="status" name="status">

    @if(isset($review))
        @if($review->status == 0 )
      <option value="0" selected>Desabilitado</option> 
      <option value="1">Habilitar</option>
        @else
      <option value="0">Desabilitar</option> 
      <option value="1" selected>Habilitado</option>
        @endif
    @else
      <option disabled selected>-- Selecione o Status --</option> 
      <option value="0">Desabilitado</option> 
      <option value="1">Habilitado</option>
    @endif
  </select>



  @if ($errors->has('status'))
  <span class="help-block">
    <strong>{{ $errors->first('status') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="row">
  <div class="col-md-6">
    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>Salvar</button>
    <a href="{{ route('reviews.index') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i>Cancelar</a>
  </div><!-- col-md-6 -->
</div>