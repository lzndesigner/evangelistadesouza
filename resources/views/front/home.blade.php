@extends('front.base')
@section('title', '')
@section('jsPage')
@endsection

@section('content')
<!-- START CONTAINER -->


@if(isset($sliders))
<section id="slider" class="slider-parallax" style="background-color: #FFF;">

    <div id="oc-slider" class="owl-carousel carousel-widget" data-margin="0" data-items="1" data-pagi="false" data-loop="true" data-animate-in="fadeIn" data-speed="450" data-animate-out="fadeOut" data-autoplay="7000">
        @foreach($sliders as $slider)
        <a href="#"><img src="/storage/{{ $slider->image }}" alt="{{ $slider->title}}"></a>
        @endforeach
    </div>

</section>
@endif

<div class="promo promo-dark nomargin notopborder">
    <div class="container clearfix">
        <h1>Fale com nossos profissionais</h1>
        <span>Tire suas dúvidas sobre nossos serviços</span>
        <a href="/contato" class="button button-border button-rounded button-white button-light button-xlarge">Fale Conosco</a>
    </div>
</div>

@if(isset($offices))
<div class="section nomargin notopborder dark background-section-sobre">
    <div class="container clearfix">

        <div class="row">

            <div class="col-md-6 ">
            </div>

            <div class="col-md-6 bottommargin">
                <div class="section_escritorio">
                    <h2>Evangelista de Souza Advogados</h2>
                    <h1>Escritório com experiência</h1>
                    <p>{!! str_replace('{', ' ', str_limit($offices->page_description, 270, '...')) !!}</p>
                    <a href="/escritorio" class="button button-border button-rounded button-white button-light button-medium">Saiba mais</a>
                </div>
            </div>

        </div>

    </div>
</div>
@endif


<section id="oc-diferencias" class="section light">
    <div class="container">

        <div class="heading-block center">
            <h2>Conheça nossos diferencias</h2>
            <span>Temos o que você precisa para atendermos a sua necessidade</span>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="feature-box fbox-center fbox-dark fbox-plain">
                    <div class="fbox-icon">
                      <img src="{{ asset('front/images/diferencias/target.png')}}" alt="">
                  </div>
                  <h3>Atuação <br> estratégica</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elerendi</p>
              </div><!-- feature -->
          </div><!-- col-md-3 -->

          <div class="col-md-3">
            <div class="feature-box fbox-center fbox-dark fbox-plain">
                <div class="fbox-icon">
                    <img src="{{ asset('front/images/diferencias/quality.png')}}" alt="">
                </div>
                <h3>Profissionais qualificados</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing eliodit disti</p>
            </div><!-- feature -->
        </div><!-- col-md-3 -->

        <div class="col-md-3">
            <div class="feature-box fbox-center fbox-dark fbox-plain">
                <div class="fbox-icon">
                    <img src="{{ asset('front/images/diferencias/trophy.png')}}" alt="">
                </div>
                <h3>Mais de 10 anos <br> de experiência</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit maiores del</p>
            </div><!-- feature -->
        </div><!-- col-md-3 -->

        <div class="col-md-3">
            <div class="feature-box fbox-center fbox-dark fbox-plain">
                <div class="fbox-icon">
                    <img src="{{ asset('front/images/diferencias/customer.png')}}" alt="">
                </div>
                <h3>Atendimento personalizado</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
            </div><!-- feature -->
        </div><!-- col-md-3 -->

    </div><!-- row -->
</div><!-- container -->
</section>

<section id="oc-areas" class="section">
    <div class="container">
        <div class="heading-block center">
            <h2>Áreas de Atuação</h2>
        </div>

        <div id="oc-areas" class="owl-carousel owl-carousel-full image-carousel carousel-widget" data-margin="30" data-loop="false" data-nav="false" data-autoplay="10000" data-pagi="true" data-items-xxs="1" data-items-xs="1" data-items-sm="2" data-items-md="3" data-items-lg="3" style="padding: 20px 0;">


            <div class="oc-item">
                <div class="feature-box media-box">
                    <div class="fbox-media">
                        <a href="/"><img src="storage/areas/1.jpg" alt="Direito Trabalhista"></a>
                    </div>
                    <div class="fbox-desc">
                        <h3><a href="/">Direito Trabalhista</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam et numquam cupiditate quas consectetur suscipit vel.</p>
                    </div>
                </div>
            </div><!-- oc-item -->

            <div class="oc-item">
                <div class="feature-box media-box">
                    <div class="fbox-media">
                        <a href="/"><img src="storage/areas/2.jpg" alt="Direito Previdenciário"></a>
                    </div>
                    <div class="fbox-desc">
                        <h3><a href="/">Direito Previdenciário</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam et numquam cupiditate quas consectetur suscipit vel.</p>
                    </div>
                </div>
            </div><!-- oc-item -->

            <div class="oc-item">
                <div class="feature-box media-box">
                    <div class="fbox-media">
                        <a href="/"><img src="storage/areas/3.jpg" alt="Direito Imobiliário"></a>
                    </div>
                    <div class="fbox-desc">
                        <h3><a href="/">Direito Imobiliário</a></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam et numquam cupiditate quas consectetur suscipit vel.</p>
                    </div>
                </div>
            </div><!-- oc-item -->

        </div><!-- carrousel -->

        <hr class="invisible">

        <div class="center"><a href="/atuacao" class="button button-rounded button-yellow button-light button-medium">Todas áreas de Atuação</a></div>

    </div><!-- container -->
</section>

@if(isset($reviews))
<section class="section topmargin-sm nomargin background-depoimentos-clientes">

    <div class="fslider testimonial testimonial-full" data-animation="fade" data-arrows="false">
        <div class="flexslider">
            <div class="slider-wrap">
                @foreach($reviews as $review)
                <div class="slide">
                    <div class="testi-content">
                        <h1 class="center">{{ $review->body }}</h1>
                        <div class="testi-meta">
                            {{ $review->name }}
                            <span>{{ $review->charge }}</span>
                        </div>
                    </div>
                </div><!-- slide -->
                @endforeach

            </div>
        </div>
    </div><!-- fslider -->
</section>
@endif

<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8">

                <div class="fancy-title title-dotted-border">
                    <h3>Entre em Contato</h3>
                </div>

                <div class="contact-widget">

                    <div class="contact-form-result"></div>

                    <form-contato></form-contato>
                </div><!-- contact-widget -->

            </div><!-- col-xs-12 col-md-6 -->

            <div class="col-xs-12 col-md-4 center">
                <img src="{{ asset('galerias/paginas/escritorio.png')}}" alt="Escritório" class="thumbnail ">
            </div>
        </div><!-- row -->

    </div><!-- container -->
</section>

<!-- END CONTAINER -->
@endsection