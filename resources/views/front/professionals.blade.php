@extends('front.base')
@section('title', 'Profissionais')

@section('breadcrumb')
<div class="clearfix"></div>
<section id="page-title">

  <div class="container clearfix">
    <h1>@yield('title')</h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>

</section>
<div class="clearfix"></div>
@endsection
@section('content')
<!-- START CONTAINER -->
<section id="oc-professionals" class="section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="heading-block center">
          <h1>Advogados especialistas preparados</h1>
          <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, sequi, adipisci? Dignissimos ad, eius enim animi doloribus, aut illum, deleniti exercitationem, eligendi corporis incidunt odit reprehenderit sint. Assumenda, atque, consectetur.</span>
        </div><!-- heading-block -->
      </div><!-- col-md- 12 -->
    </div><!-- row -->
  </div><!-- container -->


  <div class="row">
    <div class="col-md-12">

      @if(count($professionals) > 0)
      @foreach($professionals as $professional)
      <div class="team team-list">
        <div class="container">
          <div class="team-image">
            <img src="/storage/{{ $professional->image }}" alt="{{ $professional->name }}">
          </div>
          <div class="team-desc">
            <div class="team-title"><h4>{{ $professional->name }}</h4><span>{{ $professional->oab }}</span></div>
            <div class="team-content">
              {!! $professional->body !!}
            </div>
          </div>
        </div><!-- container -->
      </div><!-- end team -->
      @endforeach
      @else
      <div class="container">
        <div class="alert alert-info"><p>Não há profissionais cadastrados no momento.</p></div>
      </div>
      @endif


    </div>
  </div>
  
</section>
<!-- END CONTAINER -->
@endsection