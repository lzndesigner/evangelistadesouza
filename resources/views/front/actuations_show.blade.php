@extends('front.base')
@section('title', $actuation->title)

@section('breadcrumb')
<div class="clearfix"></div>
<section id="page-title">

  <div class="container clearfix">
    <h1>@yield('title')</h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>

</section>
<div class="clearfix"></div>
@endsection
@section('content')
<!-- START CONTAINER -->
<section class="section page-quem-somos">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-7">

          <div class="description">
            {!! str_replace('{', ' ', $actuation->body) !!}
          </div>

        </div><!-- col-md-7 -->
        <div class="col-md-5">
          <img src="/storage/{{ $actuation->image }}" alt="{{ $actuation->title }}" class="img-responsive">
        </div><!-- col-md-5 -->
      </div><!-- col-md-12 -->
    </div><!-- row -->
  </div><!-- container -->
</section>
<div class="clearfix"></div>
<!-- END CONTAINER -->
@endsection
