@extends('front.base')
@section('title', 'Informações da Reservas')
@section('jsPage')
@endsection

@section('breadcrumb')
<div class="clearfix"></div>
<section id="page-title">

  <div class="container clearfix">
    <h1>@yield('title')</h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>

</section>
<div class="clearfix"></div>
@endsection
@section('content')
<section class="section">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-4">
        <div class="fancy-title title-dotted-border">
          <h3>Informações da Reserva</h3>
        </div>
        
        <div class="infoReservations">
            <ul>
              <li><b>Múnicipio:</b> {{ $reservation->municipio }}</li>
              <li><b>Bioma:</b> {{ $reservation->bioma }}</li>
              <li><b>Tamanho:</b> {{ $reservation->tamanho_area }} / {{ $reservation->area_tipo }}</li>
              <li><b>Situação:</b> {{ $reservation->situacao }}</li>
              <li><b>Área Georeferenciada:</b>  {{ $reservation->georeferencia }}</li>
              <li><b>Inscrição CAR:</b> {{ $reservation->inscricao_car }}</li>
            </ul>
        </div>
      </div><!-- col-xs-12 col-md-7 -->
      <div class="col-xs-12 col-md-8">
        <div class="fancy-title title-dotted-border">
          <h3>Tenho Interesse</h3>
        </div>

        {{--BEGIN: form--}}
        <front-form-interesse :reservation="{{ $reservation }}"></front-form-interesse>
        {{--END: form--}}
      </div><!-- col-xs-12 col-md-7 -->

      <a href="../" class="button button-small green nomargin">Voltar</a>

    </div><!-- row -->

    <div class="clearfix"></div>

    <!-- END CONTAINER -->
    @endsection
