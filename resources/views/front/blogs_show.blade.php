@extends('front.base')
@section('title', 'Ler Artigo')

@section('breadcrumb')
<div class="clearfix"></div>
<section id="page-title">

  <div class="container clearfix">
    <h1>@yield('title')</h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>

</section>
<div class="clearfix"></div>
@endsection
@section('content')
<!-- START CONTAINER -->
<section class="section page-quem-somos">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="heading-block ">
          <h1 class="text-primary">Ler Artigo</h1>
        </div><!-- heading-block -->
      </div><!-- col-md- 12 -->
    </div><!-- row -->

    <div class="col-xs-12 col-md-9">
      <div class="single-post nobottommargin">

        <div class="entry clearfix">

          <div class="entry-title">
            <h2>{{ $blog->title }}</h2>
          </div>

          <ul class="entry-meta clearfix">
            <li><i class="icon-calendar2"></i> {{ $blog->created_at }}</li>
            <li><i class="icon-calendar3"></i> {{ $blog->updated_at }}</li>
            <li><i class="icon-tag"></i> {{ $blog->tags }}</li>
          </ul>

          <div class="entry-image">
            <a href="/storage/{{ $blog->image }}" data-lightbox="image"><img src="/storage/{{ $blog->image }}" alt="{{ $blog->title }}"></a>
          </div>

          <div class="entry-content notopmargin">
           {!! $blog->body !!}
            <div class="clear"></div>

            <div class="si-share noborder text-right clearfix">
              <span>Compartilhe esse artigo:</span>
              <div>
                <a href="#" class="social-icon si-borderless si-facebook">
                  <i class="icon-facebook"></i>
                  <i class="icon-facebook"></i>
                </a>
                <a href="#" class="social-icon si-borderless si-twitter">
                  <i class="icon-twitter"></i>
                  <i class="icon-twitter"></i>
                </a>
                <a href="#" class="social-icon si-borderless si-pinterest">
                  <i class="icon-pinterest"></i>
                  <i class="icon-pinterest"></i>
                </a>
                <a href="#" class="social-icon si-borderless si-gplus">
                  <i class="icon-gplus"></i>
                  <i class="icon-gplus"></i>
                </a>
                <a href="#" class="social-icon si-borderless si-rss">
                 <i class="icon-rss"></i>
                 <i class="icon-rss"></i>
               </a>
               <a href="#" class="social-icon si-borderless si-email3">
                <i class="icon-email3"></i>
                <i class="icon-email3"></i>
              </a>
            </div>
          </div>
        </div>
      </div>

    </div><!-- single-post -->
  </div><!--col-md-9 -->
  <div class="col-xs-12 col-md-3">
    <h4>Outros Artigos</h4>

    <div class="widget">
      
      @foreach($blogs as $blog)
      <div class="spost clearfix">
        <div class="entry-image">
          <a href="/noticias/{{ $blog->slug }}" class="nobg"><img class="rounded-circle" src="/storage/{{ $blog->image }}" alt="{{ $blog->title }}"></a>
        </div>
        <div class="entry-c">
          <div class="entry-title">
            <h4><a href="/noticias/{{ $blog->slug }}">{{ $blog->title }}</a></h4>
          </div>
        </div>
      </div>
      @endforeach

    </div><!-- widget -->


  </div>
</div><!-- container -->
</section>
<!-- END CONTAINER -->
@endsection
