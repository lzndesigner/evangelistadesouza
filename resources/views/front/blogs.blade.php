@extends('front.base')
@section('title', 'Nosso Notícias')

@section('breadcrumb')
<div class="clearfix"></div>
<section id="page-title">

  <div class="container clearfix">
    <h1>@yield('title')</h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>

</section>
<div class="clearfix"></div>
@endsection
@section('content')
<!-- START CONTAINER -->
<section class="section page-quem-somos">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="heading-block ">
          <h1 class="text-primary">Nosso Notícias</h1>
        </div><!-- heading-block -->
      </div><!-- col-md- 12 -->
    </div><!-- row -->

    <div class="row">
      @if(count($blogs) > 0)
      @foreach($blogs as $blog)
      <div class="col-xs-12 col-md-4  bottommargin-sm">
        <div class="feature-box media-box">
          <div class="fbox-media">
            <a href="/noticias/{{ $blog->slug }}"><img src="/storage/{{ $blog->image }}" alt="{{ $blog->title }}"></a>
          </div>
          <ul class="entry-meta clearfix">
            <li><i class="icon-calendar3"></i> {{ $blog->created_at }}</li>
          </ul>
          <div class="fbox-desc">
            <h3><a href="/noticias/{{ $blog->slug }}">{{ $blog->title }}</a></h3>
            {!! str_limit($blog->body, $limit = 190, $end = '...') !!}
          </div>
        </div>
      </div><!-- col-xs-12 col-md-4 -->
      @endforeach
      @else
      <div class="alert alert-info"><p>Não há notícias cadastradas no momento.</p></div>
      @endif


    </div><!-- row -->


  </div><!-- container -->
</section>
<!-- END CONTAINER -->
@endsection
