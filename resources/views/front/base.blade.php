<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="{{ $configs->config_description ?? 'Escritório de Advogados' }}">
  <meta name="keywords" content="{{ $configs->config_keywords ?? 'evangelista, souze, escritório, advogados' }}" />
  <title>{{ $configs->config_title ?? 'Evangelista de Souza' }}</title>
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Stylesheets
    ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Poppins|Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{ asset('front/css/app.css?8') }}" type="text/css" />
  <!--[if lt IE 9]>
  <script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->

<style>
.section {
  z-index: 1;
}
.infoReservations ul{
  margin: 0;
  padding: 0;
  list-style: none;
}
.infoReservations ul li{
  display: list-item;
  font-size: 18px;
  padding: 15px 0 5px;
  border-bottom: 1px dashed #DDD;
}
.bankreservations h2 {
  font-size: 16px;
  text-align: center;
  margin: 5px 0;
}
.bankreservations img {
  width: 100%;
  height: 150px;
  border-radius: 5px;
}
@media screen and (max-width:981px){
  .bankreservations img {
    height: auto;
  }
}
</style>
@yield('cssPage')


</head>
<body class="stretched">
  <div id="app">
  <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">

      <header id="header" class="bgbreadcrumb">
        <div id="top-bar" class="d-none d-md-block">
          <div class="container clearfix">
            <div class="col_half col_last fright nobottommargin">
              <div class="top-links">
                <ul class="clearfix">
                  <li>{{$configs->config_phone}} </li>
                  @if(!empty($configs->config_cellphone))<li>{{$configs->config_cellphone}} </li> @endif

                      @if($configs->redesocial_facebook != '#')
                      <li><a href="{{ $configs->redesocial_facebook }}" data-toggle="tooltip" data-placement="bottom" data-original-title="Facebook">
                        <i class="icon-facebook"></i>
                      </a></li>
                      @endif

                      @if($configs->redesocial_instagram != '#')
                      <li><a href="{{ $configs->redesocial_instagram }}" data-toggle="tooltip" data-placement="bottom" data-original-title="Instagram">
                        <i class="icon-instagram"></i>
                      </a></li>
                      @endif

                      @if($configs->redesocial_instagram != '#')
                      <li><a href="{{ $configs->redesocial_instagram }}" data-toggle="tooltip" data-placement="bottom" data-original-title="Twitter">
                        <i class="icon-twitter"></i>
                      </a></li>
                      @endif
                </ul>
              </div>
            </div>
          </div>
        </div>
        
        <div id="header-wrap">
          <div class="container clearfix">
            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

            <div id="logo">
              <a href="{{ url('/')}}" class="standard-logo" data-dark-logo="{{ asset('logo_branca.png') }}"><img src="{{ asset('logo_preto.png') }}" alt="Evangelista de Souza"></a>
              <a href="{{ url('/')}}" class="retina-logo" data-dark-logo="{{ asset('logo_branca.png') }}"><img src="{{ asset('logo_preto.png') }}" alt="Evangelista de Souza"></a>
            </div><!-- #logo end -->

            <nav id="primary-menu">
             @include('front.includes.menu-top')
           </nav>
         </div>
       </div>

       @yield('breadcrumb')
     </header>

     
  <!-- Content
    ============================================= -->
    <section id="content">
      <div class="content-wrap">
        @yield('content')
      </div><!-- .content-wrap -->
    </div><!-- #content -->
    <!-- End Content -->


  <!-- Footer
    ============================================= -->
    <footer id="footer" class="dark">

      <div class="container">

        <div class="row">

        <!-- Footer Widgets
          ============================================= -->
          <div class="footer-widgets-wrap clearfix">

            <div class="col-xs-12 col-md-9">

              <div class="widget clearfix">
                <img src="{{ asset('logo_branca.png') }}" alt="Evangelista de Souza" class="footer-logo">
                <p>{!! str_replace('{', ' ', str_limit($offices->page_description, 320, '...')) !!}</p>

                <hr class="invisible">

                <div class="row">                    

                  <div class="col-xs-12 col-md-6">
                    <h4>Nosso Escritório</h4>
                    <div>
                      <address class="nomargin">
                        {{$configs->company_address}}
                      </address>
                      <abbr title="E-mail"><strong>E-mail:</strong></abbr> <a href="mailto:contato@evangelistadesouza.com.br">contato@evangelistadesouza.com.br</a>
                      <ul class="footer-phones">
                        <li>{{$configs->config_phone}} </li>
                        @if(!empty($configs->config_cellphone))<li> | </li> <li>{{$configs->config_cellphone}} </li> @endif
                      </ul>
                    </div>
                  </div><!-- cols -->

                  <div class="col-xs-12 col-md-6">
                    <h4>Siga-nos</h4>

                    <div class="fleft clearfix">
                      @if($configs->redesocial_facebook != '#')
                      <a href="{{ $configs->redesocial_facebook }}" data-toggle="tooltip" data-original-title="Facebook" class="social-icon si-small si-borderless si-facebook">
                        <i class="icon-facebook"></i>
                        <i class="icon-facebook"></i>
                      </a>
                      @endif

                      @if($configs->redesocial_instagram != '#')
                      <a href="{{ $configs->redesocial_instagram }}" data-toggle="tooltip" data-original-title="Instagram" class="social-icon si-small si-borderless si-instagram">
                        <i class="icon-instagram"></i>
                        <i class="icon-instagram"></i>
                      </a>
                      @endif

                      @if($configs->redesocial_instagram != '#')
                      <a href="{{ $configs->redesocial_instagram }}" data-toggle="tooltip" data-original-title="Twitter" class="social-icon si-small si-borderless si-twitter">
                        <i class="icon-twitter"></i>
                        <i class="icon-twitter"></i>
                      </a>
                      @endif

                    </div>

                  </div><!-- cols -->

                </div><!-- row -->
              </div>

            </div><!-- cols -->

            <div class="col-xs-12 col-md-3">
              <div class="widget widget_links clearfix">

                <h4>Navegação</h4>

                @include('front.includes.menu-top')

              </div>
            </div><!-- cols -->

          </div><!-- .footer-widgets-wrap end -->

        </div><!-- row -->
      </div><!-- container -->

    <!-- Copyrights
      ============================================= -->
      <div id="copyrights">

        <div class="container clearfix">

          <div class="col_half">
            Todos os direitos reservados á <b>Evangelista de Souza</b> {{ date('Y') }}
          </div>

          <div class="col_half col_last tright">
            <div class="fright clearfix">
              Programação: InnSystem | Layout: RB

            </div>
          </div>

        </div>

      </div><!-- #copyrights end -->

    </footer><!-- #footer end -->

  </div><!-- #wrapper end -->

<!-- Go To Top
  ============================================= -->
  <div id="gotoTop" class="icon-angle-up"></div>

</div>
<!-- External JavaScripts
  ============================================= -->
  <script src="{{ asset('js/app.js') }}"></script>
  <script type="text/javascript" src="{{ asset('front/js/jquery.js') }}"></script>
  <script defer src="{{ asset('front/js/fontawesome-all.js') }}"></script>
  <script type="text/javascript" src="{{ asset('front/js/plugins.js') }}"></script>
  <script type="text/javascript" src="{{ asset('front/js/functions.js') }}"></script>
  @yield('jsPage')
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBi8t-0tjEnDS6KlE5tYQUCf8szqfGOQGs&libraries=places&sensor=false"></script>
  <script type="text/javascript" src="{{ asset('front/js/jquery.gmap.js') }}"></script>
  <script type="text/javascript">
    jQuery('#google-map').gMap({
      address: '{{$configs->company_city}}, {{$configs->company_state}}, Brazil',
      maptype: 'ROADMAP',
      zoom: 14,
      markers: [
      {
        address: '{{$configs->company_city}}, {{$configs->company_state}}, Brazil',
        icon: {
          image: "{{ asset('front/images/icons/map-icon-red.png') }}",
          iconsize: [32, 39],
          iconanchor: [32,39]
        }
      }
      ],
      doubleclickzoom: false,
      controls: {
        panControl: true,
        zoomControl: true,
        mapTypeControl: true,
        scaleControl: false,
        streetViewControl: false,
        overviewMapControl: false
      }
    });
  </script>
</body>
</html>