@extends('front.base')
@section('title', 'O Escritório')

@section('breadcrumb')
<div class="clearfix"></div>
<section id="page-title">

  <div class="container clearfix">
    <h1>@yield('title')</h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>

</section>
<div class="clearfix"></div>
@endsection
@section('content')
<!-- START CONTAINER -->
<section id="oc-escritorio" class="section">
  <div class="container">
    <div class="row">
      <div class="col-md-5 center">
        <div class="description">
          {!! str_replace('{', ' ', $offices->page_description) !!}
        </div>
      </div><!-- col-md-5 -->

      <div class="col-md-7">
          <ul id="lightSlider">
            @foreach($office_photos as $office_photo)
            <li data-thumb="/storage/{{ $office_photo->name }}">
              <img src="/storage/{{ $office_photo->name }}" />
            </li>
            @endforeach
          </ul>
      </div><!-- col -->

  </div><!-- row -->

  <div class="col-md-12 topmargin-sm">
    <div class="linksoffices">
      <ul>
        <li><a href="/atuacao" class="button button-3d button-large button-rounded button-primary button-light">Nossas áreas de atuação</a></li>
        <li><a href="/profissionais" class="button button-3d button-large button-rounded button-black button-light">Conheça nossos profissionais</a></li>
      </ul>
    </div>
  </div>

</div><!-- row -->
</div><!-- container -->
</section>
<div class="clearfix"></div>

<!-- END CONTAINER -->
@endsection

@section('cssPage')
<link type="text/css" rel="stylesheet" href="/front/css/lightslider.css?1" />
@endsection
@section('jsPage')
<script src="/front/js/lightslider.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $("#lightSlider").lightSlider({
     gallery: true,
     item: 1,
     loop: true,
     slideMargin: 0,
     thumbItem: 8,

     onBeforeStart: function (el) {},
     onSliderLoad: function (el) {},
     onBeforeSlide: function (el) {},
     onAfterSlide: function (el) {},
     onBeforeNextSlide: function (el) {},
     onBeforePrevSlide: function (el) {}
   });
  });
</script>
@endsection