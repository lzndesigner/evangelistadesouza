<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <title>Evangelista de Souza - Advogados</title>

  <meta name="description" content="" />
  <meta name="keywords" content= "" />
  <meta name="robots" content="follow" />
  <meta name="googlebot" content="index, follow, all" />
  <meta name="language" content="pt-br" />
  <meta name="revisit-after" content="3 days">
  <meta name="rating" content="general" />
  <meta property="og:locale" content="pt_BR"/>
  <meta property="og:type" content="website"/>

    <link href="/favicon.ico" rel="icon" />


  <meta property="og:image:url" content="/facebook.jpg" />
  <meta property="og:image:type" content="image/jpeg" />
  <meta property="og:description" content=""/>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />

  <style>
  *{margin:0; padding:0;}
  body{
    background: #000 url(/public/bg.jpg) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    font-size:12px;
    font-family: 'Open Sans', Arial, Tahoma;
    color:#FFF;
  }
  a{
    text-decoration: none;
  }

  .center{
    width: 80%;
    margin:0 auto;
    text-align: center;
  }
  .center h2 {
    font-size: 80px;
    color: #957c41;
    margin: 90px 0 20px 0;
  }
  .img-responsive{
    display: block;
    max-width: 70%;
    margin: 0 auto;
    height: auto;
    margin-top:50px;
  }
  .redesociais{
    width: 100%;
    float: left;
    margin-top:50px;
  }
  .redesociais ul{
    margin: 0;
    padding: 0;
    list-style: none;
    text-align: center;
  }
  .redesociais ul li{
    display: inline-block;
    padding: 10px 20px;
    color: #FFF;
    font-size: 23px;
  }
  .redesociais ul li b{
    color: #FFF;
    font-weight: bold;
  }
  .redesociais ul li a{
    color: #FFF;
    font-size: 23px;
    text-decoration: none;
  }
  .redesociais ul li a:hover{
    color:#fec30d;
  }

  .redesociais ul li span {
    float: left;
    background: #957c41;
    text-align: center;
    padding: 10px 15px;
    margin: 7px 15px 0 0px;
    border-radius: 4px;
  }
  .redesociais ul li div {
    float: left;
    padding: 0 0 0 10px;
    text-align:left;
  }
</style>

</head>
<body>

  <div class="center">
    <img src="{{ asset('logo_branca.png') }}" alt="Evangelista de Souza - Advogados" class="img-responsive" style="width:350px;">

    <h2>Site em Construção!</h2>

    <div class="redesociais">
      <ul>
        <li><span><i class="fa fa-phone"></i></span> <div> (16) 3234-1597 - (16) 3421-4983 <br> (16) 3235-6315 - (16) 3289-1483</div></li>
        <li><span><i class="fa fa-map-marker"></i></span> <div>Rua Elias Farah Badra, 714 - Nova Ribeirânia <br>CEP: 14096-600 - Ribeirão Preto/SP</div></li>
      </ul>
    </div>
  </div>
  
</body>
</html>