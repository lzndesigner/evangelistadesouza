@extends('front.base')
@section('title', 'Bioma: Amazônia')
@section('jsPage')
@endsection

@section('breadcrumb')
  <div class="clearfix"></div>
  <section id="page-title">

    <div class="container clearfix">
      <h1>@yield('title')</h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Início</a></li>
        <li class="active">@yield('title')</li>
      </ol>
    </div>

  </section>
  <div class="clearfix"></div>
@endsection
@section('content')
  <section class="section">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-12">
          <div class="fancy-title title-dotted-border">
            <h3>Bioma: {{ $bioma }}</h3>
          </div>
          <div class="row">
            <div class="col-md-5">
              <img src="/storage/reservas/tipos/{{ $bioma }}.jpg" alt="{{ $bioma }}" class="img-responsive">
            </div>
            <div class="col-md-7">
              <p>A Amazônia é um importante bioma com território que corresponde a 6,9 milhões de Km² e abrange nove países: Brasil, Bolívia, Colômbia, Equador, Venezuela, Guiana, Guiana Francesa, Peru, Suriname.</p>

              <p>A parte brasileira equivale a 4.196.943 milhões de Km², sendo o maior bioma brasileiro.</p>

              <p>Além do seu vasto território, uma outra característica que impressiona é a sua biodiversidade. Na Amazônia existem, aproximadamente, 2.500 espécies de árvores e cerca de 30 mil espécies de plantas, das 100 mil existentes em toda a América do Sul.</p>

              <p>A floresta amazônica é considerada a maior floresta tropical do mundo e a sua conservação é tema de discussões e financiamentos internacionais, especialmente pela sua importância na regulação da climática global.</p>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="fancy-title title-dotted-border">
            <h3>Reservas Cadastradas</h3>
          </div>

          {{--BEGIN list --}}
          <front-list-reservations :list="{{ $reservations }}" bioma="{{ $bioma }}"></front-list-reservations>
          {{--END list --}}
        </div><!-- col-xs-12 col-md-6 -->

        <a href="../" class="button button-small green nomargin">Voltar</a>

      </div><!-- row -->

      <div class="clearfix"></div>

      <!-- END CONTAINER -->
@endsection
