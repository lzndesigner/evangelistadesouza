@extends('front.base')
@section('title', 'Atuação')

@section('breadcrumb')
<div class="clearfix"></div>
<section id="page-title">

  <div class="container clearfix">
    <h1>@yield('title')</h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>

</section>
<div class="clearfix"></div>
@endsection
@section('content')
<!-- START CONTAINER -->
<section id="oc-actuations" class="section">
  <div class="container">

    <div class="heading-block center">
      <h2>Conheça nossas áreas de atuação</h2>
      <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus.</span>
    </div>

    <div class="row">
    @if(count($actuations) > 0)
    @foreach($actuations as $actuation)
    <div class="col-xs-12 col-md-4 bottommargin-sm">
      <div class="feature-box media-box">
        <div class="fbox-media">
          <a href="/atuacao/{{ $actuation->slug }}"><img src="/storage/{{ $actuation->image }}" alt="{{ $actuation->title }}"></a>
        </div>
        <div class="fbox-desc">
          <h3><a href="/atuacao/{{ $actuation->slug }}">{{ $actuation->title }}</a></h3>
          <p>{!! str_limit($actuation->body, $limit = 190, $end = '...') !!}</p>
        </div>
      </div>
    </div><!-- col -->
    @endforeach
      @else
        <div class="alert alert-info"><p>Não há profissionais cadastrados no momento.</p></div>
      @endif


    </div>

  </div><!-- container -->
</section>
<!-- END CONTAINER -->
@endsection
