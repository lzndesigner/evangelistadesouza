<ul>
  <li class=""><a href="{{ url('/') }}"><div>Home</div></a></li>
  <li class=""><a href="{{ url('/escritorio') }}"><div>O Escritório</div></a></li>
  <li class=""><a href="{{ url('/atuacao') }}"><div>Atuação</div></a>
    @if(count($actuations) >= 1)
  	<ul>
      @foreach($actuations as $actuation)
  		<li><a href="/atuacao/{{ $actuation->slug }}">{{ $actuation->title }}</a></li>
      @endforeach
  	</ul>
    @endif
  </li>
  <li class=""><a href="{{ url('/profissionais') }}"><div>Profissionais</div></a></li>
  <li class=""><a href="{{ url('/noticias') }}"><div>Notícias</div></a></li>
  <li class=""><a href="{{ url('/contato') }}"><div>Contato</div></a></li>
</ul>