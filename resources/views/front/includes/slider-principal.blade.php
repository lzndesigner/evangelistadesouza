<section id="slider" class="slider-parallax revslider-wrap clearfix">
  <div class="slider-parallax-inner">
        <!--
        #################################
          - THEMEPUNCH BANNER -
        #################################
      -->
      <div class="tp-banner-container rev_slider_wrapper fullscreen-container">
        <div class="tp-banner" >
          <ul>    <!-- SLIDE  -->
            @foreach($sliders as $slider)
            <li class="dark" data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="/storage/{{ $slider->image }}"  data-saveperformance="off"  data-title="Welcome to Canvas">
              <!-- MAIN IMAGE -->
              <img src="/storage/{{ $slider->image }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
              <!-- LAYERS -->

            <!-- LAYER NR. 2 -->
            <div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text uppercase"
            data-x="350"
            data-y="25"
            data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
            data-speed="800"
            data-start="1000"
            data-easing="easeOutQuad"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.01"
            data-endelementdelay="0.1"
            data-endspeed="1000"
            data-endeasing="Power4.easeIn" style="z-index: 3; white-space: nowrap;">{{ $slider->title }}</div>

            <div class="tp-caption customin ltl tp-resizeme revo-slider-emphasis-text nopadding noborder"
            data-x="146"
            data-y="55"
            data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
            data-speed="800"
            data-start="1200"
            data-easing="easeOutQuad"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.01"
            data-endelementdelay="0.1"
            data-endspeed="1000"
            data-endeasing="Power4.easeIn" style="z-index: 3; white-space: nowrap;">{{ $slider->subtitle }}</div>


            <!-- LAYER NR. 1
            <div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text uppercase"
            data-x="170"
            data-y="170"
            data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
            data-speed="800"
            data-start="1600"
            data-easing="easeOutQuad"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.01"
            data-endelementdelay="0.1"
            data-endspeed="1000"
            data-endeasing="Power4.easeIn" style="z-index: 3; white-space: nowrap;"><iframe width="760" height="415" src="https://www.youtube.com/embed/_ReQ3jWEbvo?rel=0&amp;showinfo=0&autoplay=1&mute=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div> -->


            <div class="tp-caption customin ltl tp-resizeme"
            data-x="310"
            data-y="630"
            data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
            data-speed="800"
            data-start="1900"
            data-easing="easeOutQuad"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.01"
            data-endelementdelay="0.1"
            data-endspeed="1000"
            data-endeasing="Power4.easeIn" style="z-index: 3; white-space: nowrap;"><a href="/reservas" class="gotoContent button button-border button-white button-light button-large button-rounded tright nomargin"><span>Cadastrar Área</span> <i class="icon-angle-right"></i></a></div>

            <div class="tp-caption customin ltl tp-resizeme"
            data-x="620"
            data-y="630"
            data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
            data-speed="800"
            data-start="2000"
            data-easing="easeOutQuad"
            data-splitin="none"
            data-splitout="none"
            data-elementdelay="0.01"
            data-endelementdelay="0.1"
            data-endspeed="1000"
            data-endeasing="Power4.easeIn" style="z-index: 3; white-space: nowrap;"><a href="#" class="gotoContent button button-border button-white button-light button-large button-rounded tright nomargin"><span>Buscar Área</span> <i class="icon-angle-right"></i></a></div>


          </li>
          @endforeach
        </ul>

      </div>
    </div>
  </div>
</section>