@extends('front.base')
@section('title', 'Contato')

@section('breadcrumb')
  <div class="clearfix"></div>
  <section id="page-title">

    <div class="container clearfix">
      <h1>@yield('title')</h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Início</a></li>
        <li class="active">@yield('title')</li>
      </ol>
    </div>

  </section>
  <div class="clearfix"></div>
@endsection
@section('content')
  <section class="section" id="oc-contact">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-8">

          <div class="fancy-title title-dotted-border">
            <h3>Entre em Contato</h3>
          </div>

          <div class="contact-widget">

            <div class="contact-form-result"></div>

            <form-contato></form-contato>
          </div><!-- contact-widget -->

        </div><!-- col-xs-12 col-md-6 -->

        <div class="col-xs-12 col-md-4 center">
          <img src="{{ asset('galerias/paginas/escritorio.png')}}" alt="Escritório" class="thumbnail ">
        </div>

      </div><!-- row -->

      <div class="clearfix"></div>

      <div class="row clear-bottommargin">
        <div class="col-xs-12 col-sm-4 col-md-3 bottommargin clearfix">
          <div class="feature-box fbox-center fbox-bg fbox-plain">
            <div class="fbox-icon">
              <a href="#"><i class="icon-phone3"></i></a>
            </div>
            <h3>Telefone<span class="subtitle">{{$configs->config_phone}} @if(!empty($configs->config_cellphone))<br> {{$configs->config_cellphone}} @endif</h3>
          </div>
        </div>

        <div class="col-xs-12 col-sm-4 col-md-5 bottommargin clearfix">
          <div class="feature-box fbox-center fbox-bg fbox-plain">
            <div class="fbox-icon">
              <a href="#"><i class="icon-envelope"></i></a>
            </div>
            <h3>E-mail<span class="subtitle"><a href="mailto:{{$configs->config_email}}">{{$configs->config_email}}</a></span></h3>
          </div>
        </div>

        <div class="col-xs-12 col-sm-4 col-md-4 bottommargin clearfix">
          <div class="feature-box fbox-center fbox-bg fbox-plain">
            <div class="fbox-icon">
              <a href="#"><i class="icon-map-marker2"></i></a>
            </div>
            <h3>Escritório<span class="subtitle">{{$configs->company_address}}, {{$configs->company_city}}/SP</span></h3>
          </div>
        </div>
      </div>
    </div><!-- container -->
  </section>

  @include('components.mapa')
  <!-- END CONTAINER -->
@endsection
