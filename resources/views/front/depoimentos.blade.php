@extends('front.base')
@section('title', 'Depoimentos')

@section('breadcrumb')
  <div class="clearfix"></div>
  <section id="page-title">

    <div class="container clearfix">
      <h1>@yield('title')</h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Início</a></li>
        <li class="active">@yield('title')</li>
      </ol>
    </div>

  </section>
  <div class="clearfix"></div>
@endsection

@section('content')
<!-- START CONTAINER -->
<section class="section page-quem-somos">
  <div class="container">
    <div class="row">

      <div class="col-xs-12 col-md-12">
        <div class="col-xs-12 col-md-7">
         <div class="fancy-title title-dotted-border">
          <h3>Envie seu depoimento</h3>
        </div>

        <div class="contact-widget">

          @include('elements.messages')

          <form class="nobottommargin" action="/depoimentos-create" method="POST">

            {!! csrf_field() !!}

            <div class="form-group @if ($errors->has('name')) has-error @endif">
              <label for="name" class="form-label">Nome do Cliente</label>
              <input type="text" class="form-control" id="name" name="name" value="{{ $review->name ?? old('name') }}" placeholder="Nome do Cliente" autofocus>
              @if ($errors->has('name'))
              <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
              @endif
            </div><!-- form-group -->

            <div class="form-group @if ($errors->has('charge')) has-error @endif">
              <label for="charge" class="form-label">Cargo do Cliente</label>
              <input type="text" class="form-control" id="charge" name="charge" value="{{ $review->charge ?? old('charge') }}" placeholder="charge do Cliente">
              @if ($errors->has('charge'))
              <span class="help-block">
                <strong>{{ $errors->first('charge') }}</strong>
              </span>
              @endif
            </div><!-- form-group -->

            <div class="form-group @if ($errors->has('body')) has-error @endif">
              <label for="body" class="form-label">Depoimento do Cliente</label>
              <textarea class="form-control" rows="4" id="body" name="body" value="{{ $review->body ?? old('body') }}" placeholder="body do Cliente">{{ $review->body ?? old('body') }}</textarea>
              @if ($errors->has('body'))
              <span class="help-block">
                <strong>{{ $errors->first('body') }}</strong>
              </span>
              @endif
            </div><!-- form-group -->

            <input type="hidden" name="status" id="status" value="0">


            <div class="col_full">
              <button type="submit" class="button button-3d nomargin">Enviar mensagem</button>
            </div>

          </form>
        </div><!-- contact-widget -->
      </div><!-- col-md-7 -->
      <div class="col-xs-12 col-md-5">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat alias optio repellendus recusandae odio praesentium dignissimos magni, culpa voluptatum nostrum obcaecati repellat quos, quaerat eveniet, saepe maiores facere libero delectus?</p>
        <img src="{{ asset('galerias/paginas/depoimentos.jpg')}}" alt="Depoimentos">
      </div><!-- col-md-5 -->
    </div><!-- col-md-12 -->

  </div><!-- row -->

</div><!-- container -->
</section>
<!-- END CONTAINER -->
@endsection
