@extends('front.base')
@section('title', 'Banco de Reservas')
@section('jsPage')
@endsection

@section('breadcrumb')
<div class="clearfix"></div>
<section id="page-title">

  <div class="container clearfix">
    <h1>@yield('title')</h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>

</section>
<div class="clearfix"></div>
@endsection
@section('content')
<section class="section">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-12">
        <div class="fancy-title title-dotted-border">
          <h3>Banco de Reservas</h3>
        </div>

        <div class="row bankreservations">
          <div class="col-md-2"><a href="{{ route('bank.show', ['name' => 'amazonia']) }}">
            <img src="{{ asset('storage/reservas/tipos/amazonia.jpg') }}" alt="Amazônia" class="img-responsive"><h2>Amazônia</h2></a>
          </div>
          <div class="col-md-2"><a href="{{ route('bank.show', ['name' => 'caatinga']) }}">
            <img src="{{ asset('storage/reservas/tipos/caatinga.jpg') }}" alt="Caatinga" class="img-responsive"><h2>Caatinga</h2></a>
          </div>
          <div class="col-md-2"><a href="{{ route('bank.show', ['name' => 'cerrado']) }}">
            <img src="{{ asset('storage/reservas/tipos/cerrado.jpg') }}" alt="Cerrado" class="img-responsive"><h2>Cerrado</h2></a>
          </div>
          <div class="col-md-2"><a href="{{ route('bank.show', ['name' => 'mataatlantica']) }}">
            <img src="{{ asset('storage/reservas/tipos/mataatlantica.jpg') }}" alt="Mata Atlântica" class="img-responsive"><h2>Mata Atlântica</h2></a>
          </div>
          <div class="col-md-2"><a href="{{ route('bank.show', ['name' => 'pampa']) }}">
            <img src="{{ asset('storage/reservas/tipos/pampa.jpg') }}" alt="Pampa" class="img-responsive"><h2>Pampa</h2></a>
          </div>
          <div class="col-md-2"><a href="{{ route('bank.show', ['name' => 'pantanal']) }}">
            <img src="{{ asset('storage/reservas/tipos/pantanal.jpg') }}" alt="Pantanal" class="img-responsive"><h2>Pantanal</h2></a>
          </div>
        </div>

      </div><!-- col-xs-12 col-md-6 -->

    </div><!-- row -->

    <div class="clearfix"></div>

    <!-- END CONTAINER -->
    @endsection
