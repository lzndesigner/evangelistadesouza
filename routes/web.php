<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/webmail', function(){ return redirect('http://webmail.innsystem.com.br/'); });
Route::get('/admin', function(){ return redirect('/dashboard'); });


 Route::get('/', 'Front\HomeController@maintence');
 Route::get('/manutencao', 'Front\HomeController@index');
 Route::get('escritorio', 'Front\OfficeController@index');
 Route::get('atuacao', 'Front\ActuationsController@index');
 Route::get('atuacao/{slugAtuacao}', 'Front\ActuationsController@show');
 Route::get('/profissionais', 'Front\ProfessionalsController@index');

 Route::get('noticias', 'Front\BlogsController@index');
 Route::get('noticias/{slugBlog}', 'Front\BlogsController@show');
 Route::get('/depoimentos', 'Front\ReviewController@index');
 Route::post('/depoimentos-create', 'Front\ReviewController@store');
 Route::get('/contato', 'Front\ContactController@index');


// Route::get('/reservas', 'Front\ReservationController@index');
// Route::get('/banco-reservas', 'Front\ReservationController@view')->name('bank.index');
// Route::get('/banco-reservas/{name}', 'Front\ReservationController@show')->name('bank.show');
// Route::get('/banco-reservas/{name}/info/{reservation}', 'Front\ReservationController@info')->name('bank.info');

 Route::prefix('services')->namespace('Services')->group(function() {
     Route::post('send-mail', 'MailController@sendContact');
//     Route::prefix('newsletter')->group(function() {
//         Route::post('/', 'MailController@subscribe');
//         Route::get('confirm/{code}', 'MailController@confirmSubscribe');
//     });
//     Route::apiResource('pre-registration', 'PreRegistrationController');
//     Route::get('reservations', 'ReservationsController@index');
//     Route::delete('reservations/{reservation}', 'ReservationsController@destroy');
//     Route::match(['patch','put'], 'reservations', 'ReservationsController@update');

//     Route::post('bank-reservations/{reservation}/interest', 'BankReservationController@interest');
//     Route::delete('bank-reservations/interest/{reservation_interest}', 'BankReservationController@destroy');
//     Route::match(['patch','put'],'bank-reservations/interest/{reservation_interest}', 'BankReservationController@update');
 });


Route::prefix('dashboard')->middleware(['auth'])->group(function(){
	Route::get('/', 'Admin\DashboardController@index');
	Route::resource('users', 'Admin\UsersController');
	Route::resource('pages', 'Admin\PagesController');
	Route::resource('configs', 'Admin\ConfigController');
    Route::resource('office', 'Admin\OfficeController');
    Route::get('office-photos', ['as' => 'office-photos', 'uses' => 'Admin\OfficeController@photos']);
    Route::post('office-upload', ['as' => 'office-upload', 'uses' => 'Admin\OfficeController@upload']);
    Route::get('office-download/{officeId}/{photoId}', ['as' => 'office-download', 'uses' => 'Admin\OfficeController@download']);
	Route::get('office-destroy/{officeId}/{photoId}', ['as' => 'office-destroy', 'uses' => 'Admin\OfficeController@destroy']);


	Route::resource('actuations', 'Admin\ActuationsController');
	Route::resource('professionals', 'Admin\ProfessionalsController');
    Route::resource('blogs', 'Admin\BlogsController');
    Route::resource('reviews', 'Admin\ReviewsController');


    Route::resource('sliders', 'Admin\SlidersController');
    Route::resource('clients', 'Admin\ClientsController');
	Route::prefix('newsletter')->group(function() {
        Route::get('list', 'Admin\NewsletterController@index');
        Route::delete('/{newsletter}', 'Admin\NewsletterController@destroy');
        Route::get('generate-list', 'Admin\NewsletterController@generate');
        Route::view('/', 'admin.newsletter.index')->name('newsletter.index');
    });
    Route::view('pre-registrations', 'admin.pre-registrations.index')->name('pre-registrations.index');
    Route::get('reservations', 'Admin\ReservationsController@index')->name('reservations.index');
    Route::match(['GET', 'POST'], 'reservations/new', 'Admin\ReservationsController@insert')->name('reservations.insert');
    Route::match(['GET', 'PUT'],'reservations/{reservation}', 'Admin\ReservationsController@viewOrUpdate')->name('reservations.viewOrUpdate');
    Route::get('reservations/{id}/ficha', 'Admin\ReservationsController@create')->name('reservations.create');
    Route::post('reservations/{id}/ficha', 'Admin\ReservationsController@store')->name('reservations.store');
});

Auth::routes();
