<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pre_registration_id')->nullable();
            $table->foreign('pre_registration_id')->references('id')->on('pre_registrations')->onDelete('cascade');
            $table->string('propietario');
            $table->string('fazenda');
            $table->string('matricula');
            $table->string('municipio');
            $table->string('municipio_cartorio');
            $table->string('estado');
            $table->string('bioma');
            $table->enum('situacao', ['regular', 'irregular'])->default('regular');
            $table->enum('georeferencia', ['sim', 'nao'])->default('nao');
            $table->string('inscricao_car')->nullable();
            $table->string('tamanho_area');
            $table->string('area_tipo');
            $table->string('descricao')->nullable();
            $table->boolean('ativo')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations', function(Blueprint $table) {
            $table->dropForeign('reservations_pre_registration_id_foreign');
            $table->dropIfExists();
        });
    }
}
