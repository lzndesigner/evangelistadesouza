<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('config_title');
            $table->text('config_description');
            $table->string('config_keywords')->nullable;
            $table->string('config_email');
            $table->string('config_phone');
            $table->string('config_cellphone')->nullable;
            $table->string('config_information')->nullable;
            $table->string('company_name');
            $table->string('company_proprietary');
            $table->string('company_address')->nullable;
            $table->string('company_city');
            $table->string('company_state');
            $table->string('company_country');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configs');
    }
}
