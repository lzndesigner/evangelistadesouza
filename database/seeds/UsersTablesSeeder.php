<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;

class UsersTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        User::create([
            'name'=>'Leonardo Augusto',
            'email'=>'contato@innsystem.com.br',
            'password'=>bcrypt('123456'),
            'level'=>'admin',
            'remember_token' => str_random(10),
        ]);

        User::create([
            'name'=>'André Evangelista',
            'email'=>'contato@evangelistadesouza.com.br',
            'password'=>bcrypt('123456'),
            'level'=>'admin',
            'remember_token' => str_random(10),
        ]);

        User::create([
            'name'=>'Ana Laura',
            'email'=>'analauraboldrin@hotmail.com',
            'password'=>bcrypt('123456'),
            'level'=>'admin',
            'remember_token' => str_random(10),
        ]);
    }
}
