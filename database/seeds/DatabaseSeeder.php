<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     $this->call(UsersTablesSeeder::class);
     //$this->call(PagesTablesSeeder::class);
     $this->call(ConfigsTablesSeeder::class);
     $this->call(OfficesTablesSeeder::class);
     $this->call(ActuationsTablesSeeder::class);
     // $this->call(SlidersTablesSeeder::class);
   }
 }
