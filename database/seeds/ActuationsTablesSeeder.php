<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\Actuation;

class ActuationsTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('actuations')->delete();

        Actuation::create([
            'title' => 'Contensioso Trabalhista',
            'slug' => 'contensioso-trabalhista',
            'body' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. <br/> <h2>Contensioso Trabalhista</h2> <br/> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br/> Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus.',
            'image' => 'actuation/contensioso-trabalhista.jpg',
        ]);

        Actuation::create([
            'title' => 'Fusões e Aquisições',
            'slug' => 'fusoes-e-aquisicoes',
            'body' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. <br/> <h2>Fusões e Aquisições</h2> <br/> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br/> Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus.',
            'image' => 'actuation/fusoes-aquisicoes.jpg',
        ]);

        Actuation::create([
            'title' => 'Direito Familiar',
            'slug' => 'direito-familiar',
            'body' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. <br/> <h2>Direito Familiar</h2> <br/> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br/> Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus.',
            'image' => 'actuation/direito-familiar.jpg',
        ]);

        Actuation::create([
            'title' => 'Direito Previdenciário',
            'slug' => 'direito-previdenciario',
            'body' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. <br/> <h2>Direito Previdenciário</h2> <br/> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br/> Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus.',
            'image' => 'actuation/direito-previdenciario.jpg',
        ]);

        Actuation::create([
            'title' => 'Direito Imobiliáio',
            'slug' => 'direito-imobiliario',
            'body' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. <br/> <h2>Direito Imobiliáio</h2> <br/> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br/> Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus.',
            'image' => 'actuation/direito-imobiliario.jpg',
        ]);

        Actuation::create([
            'title' => 'Direito Criminal',
            'slug' => 'direito-criminal',
            'body' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. <br/> <h2>Direito Criminal</h2> <br/> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br/> Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim possimus blanditiis, cum recusandae voluptatum est distinctio repudiandae sint doloribus.',
            'image' => 'actuation/direito-criminal.jpg',
        ]);
    }
}
