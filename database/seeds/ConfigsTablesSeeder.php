<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\Config;

class ConfigsTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configs')->delete();

        Config::create([
            'config_title' => 'Evangelista de Souza Advogados',
            'config_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt quasi, optio placeat, incidunt libero corrupti.',
            'config_keywords' => 'evangelista, souza, advogados, evangelistadesouza, ribeirao, preto, ribeirao preto, sao paulo, oab, sao, paulo',
            'config_email' => 'contato@evangelistadesouza.com.br',
            'config_phone' => '(16) 3234-1597',
            'config_cellphone' => '(16) 3421-4983',
            'config_information' => 'Informações Complementares',
            'company_name' => 'Evangelista de Souza Advogados',
            'company_proprietary' => 'André Evangelista',
            'company_address' => 'Rua Elias Farah Badra, 714 - Nova Ribeirânia',
            'company_city' => 'Ribeirão Preto',
            'company_state' => 'São Paulo',
            'company_country' => 'BR',
            'redesocial_facebook' => '#',
            'redesocial_instagram' => '#',
            'redesocial_twitter' => '#',
        ]);
    }
}
