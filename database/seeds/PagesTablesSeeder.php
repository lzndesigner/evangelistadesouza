<?php

use Illuminate\Database\Seeder;

class PagesTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('truncate pages');
        factory(App\Page::class, 20)->create();
    }
}
