<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\Office;

class OfficesTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('offices')->delete();

        Office::create([
            'page_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt quasi, optio placeat, incidunt libero corrupti. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt quasi, optio placeat, incidunt libero corrupti.',
        ]);
    }
}
