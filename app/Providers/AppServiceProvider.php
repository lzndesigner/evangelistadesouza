<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use View;
use DB;

use App\Models\Config;
use App\Models\Office;
use App\Models\OfficePhotos;
use App\Models\Actuation;
use App\Models\Slider;
use App\Models\Blog;
use App\Models\Review;
use App\Models\Client;
use App\Models\Professional;

use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        
        if(DB::connection()->getDatabaseName())
        {
            View::share('configs', Config::first());
            View::share('offices', Office::first());
            View::share('office_photos', OfficePhotos::get());
            View::share('actuations', Actuation::get());
            View::share('sliders', Slider::get());
            View::share('blogs', Blog::get());
            View::share('reviews', Review::where('status', 1)->get());
            View::share('clients', Client::where('status', 1)->get());
            View::share('professionals', Professional::where('status', 1)->get());

            \Carbon\Carbon::setLocale('pt-br');
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
