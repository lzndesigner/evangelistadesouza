<?php

namespace App\Exports;

use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use App\Models\Newsletter;
use PhpOffice\PhpSpreadsheet\Worksheet\ColumnCellIterator;
use PhpOffice\PhpSpreadsheet\Worksheet\ColumnDimension;

class NewslettersExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithColumnFormatting
{

    /**
     * Obtem lista de registros a serem exportados para planilha
     */
    public function collection() 
    {
        return Newsletter::select('name', 'email')->where('status', 'active')->get();
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        // TODO: Implement map() method.
        return [
            $row->name,
            $row->email,
        ];
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        // TODO: Implement headings() method.
        return [
            'Nome Completo',
            'E-mail'
        ];
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        // TODO: Implement columnFormats() method.
        return [
            (new ColumnDimension('A'))->setWidth(100)->getWidth(),
            (new ColumnDimension('B'))->setWidth(100)->getWidth(),
        ];
    }
}
