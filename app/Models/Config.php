<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $fillable = [
    	'config_title',
	'config_description',
	'config_keywords',
	'config_email',
	'config_phone',
	'config_cellphone',
	'config_information',
	'company_name',
	'company_proprietary',
	'company_address',
	'company_city',
	'company_state',
	'company_country',
	'redesocial_facebook',
	'redesocial_instagram',
	'redesocial_twitter'
    ];
}
