<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    protected $fillable = [
    	'page_description'
    ];

    public function photos(){
    	return $this->hasMany('App\Models\OfficePhotos');
    }
}
