<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfficePhotos extends Model
{
    public function office(){
    	return $this->belongsTo('App\Models\Office', 'id');
    }
}
