<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Actuation extends Model
{
    protected $fillable = [
    	'title',
    	'body',
    	'slug',
    	'image'
    ];
}
