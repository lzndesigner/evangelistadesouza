<?php

namespace App\Http\Controllers\Services;

use App\Http\Resources\ReservationCollection;
use App\Models\Reservation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReservationsController extends Controller
{

    /**
     * Lista de reservas
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return ReservationCollection::collection(
            Reservation::all()
        );
    }

    /**
     * Atualizar
     *
     * @param Reservation $reservation
     */
    public function update(Reservation $reservation)
    {

        return $reservation;
    }

    /**
     * Remover
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Reservation $reservation)
    {
       $reservation->delete();

       if($request->wantsJson()) {
         return response(null, 204);
       }

       return response()->redirectToRoute('reservations.index');
    }
}
