<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use App\Models\Reservation;
use App\Models\ReservationInterest;
use Illuminate\Http\Request;

class BankReservationController extends Controller
{
	/**
	 * Cadastrat interesse em reserva
	 *
	 * @param Request $request
	 * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
	 */
	public function interest(Request $request, Reservation $reservation)
	{
		$intereset = app(ReservationInterest::class);

		$intereset->fill($request->all());
		$intereset->reservation()->associate($reservation);
		$intereset->saveOrFail();

		return response('creted', 201);
	}

	/**
	 * Remover interessado da lista de reserva
	 *
	 * @param Request $request
	 * @param ReservationInterest $interest
	 * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
	 * @throws \Exception
	 */
	public function destroy(Request $request, ReservationInterest $interest)
	{
		$interest->delete();

		return response(null, 204);
	}

	/**
	 * @param Request $request
	 * @param ReservationInterest $interest
	 */
	public function update(Request $request, ReservationInterest $interest)
	{
		$interest->update($request->all());

		return response()->json([
			'update' => $interest->getChanges()
		]);

	}
}