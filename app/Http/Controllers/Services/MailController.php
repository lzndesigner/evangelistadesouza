<?php

namespace App\Http\Controllers\Services;

use App\Http\Requests\StoreNewsletter;
use App\Mail\SupportSiteContact;
use App\Models\Newsletter;
use App\Models\NewsletterConfirmation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    /**
     * Enviar email para suporte do sistema
     *
     * @param Request $request
     */
    public function sendContact(Request $request)
    {
        // send
        $send = Mail::send(new SupportSiteContact($request->all()));

        return response(null, 204);
    }

    /**
     * Increver-se no newsletter
     *
     * @param Request $request
     * @return mixed
     */
    public function subscribe(StoreNewsletter $request)
    {
        try{
            $newsletter = new Newsletter;
            $newsletter->fill($request->all());
            $newsletter->save();
            return response('created.', 201);
        }catch(\Error $error) {
            return response()->json(['message' => $error->getMessage()], 400);
        }
    }

    /**
     * Confirmar inscrição
     *
     * @param Request $request
     */
    public function confirmSubscribe(Request $request, $code)
    {
        $confirmation = NewsletterConfirmation::where('code', base64_decode($code))->firstOrFail();

        if($request->action === 'true') {
            $confirmation->newsletter->update(['status' => 'active']);
            return redirect()->to('/')->with('message', 'Confirmou a inscrição');
        }else {
            $confirmation->newsletter->update(['status' => 'cancel']);
            return redirect()->to('/')->with('message', 'Cancelou a inscrição');
        }
    }

}
