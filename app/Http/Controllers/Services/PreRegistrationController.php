<?php

namespace App\Http\Controllers\Services;

use App\Http\Requests\PreRegisterStore;
use App\Http\Resources\PreRegisterCollection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PreRegistration;

class PreRegistrationController extends Controller
{

    /**
     * Lista de pre-cadastros
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return PreRegisterCollection::collection(PreRegistration::all());

    }

    /**
     * Atualizar registro
     *
     * @param Request $request
     * @param PreRegistration $registration
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $id)
    {
        $registration = PreRegistration::findOrFail($id);

        $registration->update(
            $request->except('subject')
        );

        return response([
            'changes' => $registration->getChanges()
        ], 204);
    }

    /**
     * Pre-cadastro
     *
     * @param PreRegisterStore $request
     * @return mixed
     */
    public function store(PreRegisterStore $request)
    {
    	try{
    		$register = new PreRegistration;
	    	$register->fill($request->all());
	    	$register->save();
	    	return response('created.', 201);
    	}catch(\Error $error) {
    		return response()->json(compact('error'), 400);
    	}
    }

    /**
     * Remover registro do banco de dados
     *
     * @param PreRegistration $registration
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $row = PreRegistration::findOrFail($id);

        if($row->delete()) {
            return response()->json(null, 204);
        }
    }
}
