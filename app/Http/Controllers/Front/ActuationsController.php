<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Actuation;

class ActuationsController extends Controller
{
	public function index()
	{
		return view('front.actuations');
	}

	public function show(Actuation $actuation)
	{
		return view('front.actuations_show', compact('actuation'));
	}
}
