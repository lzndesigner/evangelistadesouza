<?php

namespace App\Http\Controllers\Front;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Reviews\StoreRequest;
use App\Http\Requests\Admin\Reviews\UpdateRequest;
use App\Models\Review;

class ReviewController extends Controller
{
    /**
     * Armazena uma nova instancia do model Review
     *
     * @var \App\Review
     */
    private $reviews;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
        $this->reviews = app(Review::class);
    }

    public function index()
    {
        return view('front.depoimentos');
    }

    public function store(StoreRequest $request)
    {
        $review = new Review;
        $review->name = $request->name;
        $review->charge = $request->charge;
        $review->status = $request->status;
        $review->body = $request->body;

        if(!$review->save()) {
            session()->flash('messages.error', ['Houve um erro. Tente novamente!']);
            return view('front.depoimentos');
        }

        session()->flash('messages.success', ['Seu depoimento foi enviado para a moderação. Agradecemos seu depoimento!']);
        return view('front.depoimentos');
        

    }
}
