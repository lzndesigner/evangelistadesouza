<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Users\StoreRequest;
use App\Http\Requests\Admin\Users\UpdateRequest;

use App\Models\User;

class UsersController extends Controller
{
  /**
  * Armazena uma nova instancia do model User
  *
  * @var \App\User
  */
  private $users;

  /**
  * Metodo construtor.
  */
  public function __construct()
  {
    $this->users = app(User::class);
  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $users = User::paginate();
    return view('admin.users.index', compact('users'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    return view('admin.users.create');
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $user = new User;
    $password = bcrypt($request->password);
    $user->name = $request->name;
    $user->email = $request->email;
    $user->password = $password;
    $user->level = $request->level;

    if(!$user->save()) {
      session()->flash('messages.error', ['Houve um erro. Tente novamente!']);
      return redirect()->route('users.index');
    }

    session()->flash('messages.success', ['Usuário cadastrado com sucesso!']);
    return redirect()->route('users.index');
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    $user = User::findOrFail($id);
    return view('admin.users.show', compact('user'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $user = User::findOrFail($id);
    return view('admin.users.edit', compact('user'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $user = $this->users->where('id', $id)->first();

    if(!$user) {
      session()->flash('messages.error', ['Usuário não existe!']);
      return redirect()->route('users.index');
    }

    $password = bcrypt($request->password);
    $user->name = $request->name;
    $user->email = $request->email;
    $user->password = $password;
    $user->level = $request->level;
    $user->save();

    session()->flash('messages.success', ['Usuário alterado com sucesso!']);
    return redirect()->route('users.index');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  User $user
  * @return \Illuminate\Http\Response
  */
  public function destroy(Request $request, User $user)
  {
    
    if($request->user()->id === $user->id) {
      // nao remover usuario que est logado.
      return response('Não pode ser removido', 403);
    }

    $user->delete();

    return response(null, 204);
    //return response()->redirectToRoute('users.index');
  }
}
