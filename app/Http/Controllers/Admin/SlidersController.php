<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Sliders\StoreRequest;
use App\Http\Requests\Admin\Sliders\UpdateRequest;

use App\Models\Slider;

class SlidersController extends Controller
{
    /**
     * Armazena uma nova instancia do model slider
     *
     * @var \App\slider
     */
    private $sliders;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
        $this->sliders = app(Slider::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sliders = Slider::orderBy($request->input('sort', 'created_at'), 'ASC')->paginate();
        return view('admin.sliders.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sliders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $slider = new Slider;

        $foto_antiga = $slider->image;

        if(Storage::has($foto_antiga)){
          Storage::delete($foto_antiga);
        }

        $file = $request->file('image');

        if(!$file->isValid()) {
          abort(400, 'Imagem não encontrada ou não é válida');
        }

        $arquivo = $file->store('sliders');
        $slider->image = $arquivo;


        $slider->title = $request->title;
        $slider->url = $request->url;

        if(!$slider->save()) {
            session()->flash('messages.error', ['Houve um erro. Tente novamente!']);
            return redirect()->route('sliders.index');
        }

        session()->flash('messages.success', ['Slider cadastrado com sucesso!']);
        return redirect()->route('sliders.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $slider = Slider::findOrFail($id);
        return view('admin.sliders.show', compact('slider'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::findOrFail($id);
        return view('admin.sliders.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $id)
    {
         $slider = $this->sliders->where('id', $id)->first();

        if(!$slider) {
            session()->flash('messages.error', ['Slider não existe!']);
            return redirect()->route('sliders.index');
        }

      if(!Input::hasFile('image') == null){
        $foto_antiga = $slider->image;

        if(Storage::has($foto_antiga)){
          Storage::delete($foto_antiga);
        }

        $file = $request->file('image');

        if(!$file->isValid()) {
          abort(400, 'Imagem não encontrada ou não é válida');
        }

        $arquivo = $file->store('sliders');
        $slider->image = $arquivo;
      }

        $slider->title = $request->title;
        $slider->url = $request->url;
        $slider->save();

        session()->flash('messages.success', ['Slider alterado com sucesso!']);
        return redirect()->route('sliders.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::findOrFail($id);
        $slider->delete();
        return response()->redirectToRoute('sliders.index');
    }
}
