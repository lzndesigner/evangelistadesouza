<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Reviews\StoreRequest;
use App\Http\Requests\Admin\Reviews\UpdateRequest;

use App\Models\Review;

class ReviewsController extends Controller
{
    /**
     * Armazena uma nova instancia do model Review
     *
     * @var \App\Review
     */
    private $reviews;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
        $this->reviews = app(Review::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $reviews = Review::orderBy($request->input('sort', 'created_at'), 'ASC')->paginate();
        return view('admin.reviews.index', compact('reviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.reviews.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $review = new Review;
        $review->name = $request->name;
        $review->charge = $request->charge;
        $review->status = $request->status;
        $review->body = $request->body;

        if(!$review->save()) {
            session()->flash('messages.error', ['Houve um erro. Tente novamente!']);
            return redirect()->route('reviews.index');
        }

        session()->flash('messages.success', ['Depoimento cadastrado com sucesso!']);
        return redirect()->route('reviews.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $review = Review::findOrFail($id);
        return view('admin.reviews.show', compact('review'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $review = Review::findOrFail($id);
        return view('admin.reviews.edit', compact('review'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $id)
    {
         $review = $this->reviews->where('id', $id)->first();

        if(!$review) {
            session()->flash('messages.error', ['Depoimento não existe!']);
            return redirect()->route('reviews.index');
        }

        $review->name = $request->name;
        $review->charge = $request->charge;
        $review->status = $request->status;
        $review->body = $request->body;
        $review->save();

        session()->flash('messages.success', ['Depoimento alterado com sucesso!']);
        return redirect()->route('reviews.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function destroy(Request $request, Review $review)
  {

    $review->delete();

    return response(null, 204);
    //return response()->redirectToRoute('blogs.index');
  }
}
