<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



use App\Http\Requests\Admin\Config\StoreRequest;
//use App\Http\Requests\Admin\Config\UpdateRequest;



use App\Models\Config;

class ConfigController extends Controller
{
    /**
     * Armazena uma nova instancia do model Config
     *
     * @var \App\Config
     */
    private $configs;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
        $this->configs = app(Config::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $config = Config::firstOrFail();
        return view('admin.configs.index', compact('config'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $config = $this->configs->firstOrFail();

        if(!$config) {
            session()->flash('messages.error', ['Configuração não existe ou não encontrada!']);
            return redirect()->route('configs.index');
        }

        $config->config_title = $request->config_title;
        $config->config_description = $request->config_description;
        $config->config_keywords = $request->config_keywords;
        $config->config_email = $request->config_email;
        $config->config_phone = $request->config_phone;
        $config->config_cellphone = $request->config_cellphone;
        $config->config_information = ($request->config_information ? $request->config_information : '');
        $config->company_name = $request->company_name;
        $config->company_proprietary = $request->company_proprietary;
        $config->company_address = $request->company_address;
        $config->company_city = $request->company_city;
        $config->company_state = $request->company_state;
        $config->company_country = $request->company_country;
	$config->redesocial_facebook = ($request->redesocial_facebook ? $request->redesocial_facebook : '');
	$config->redesocial_instagram = ($request->redesocial_instagram ? $request->redesocial_instagram : '');
	$config->redesocial_twitter = ($request->redesocial_twitter ? $request->redesocial_twitter : '');
        $config->save();

        session()->flash('messages.success', ['Configuração Geral alterada com sucesso!']);
        return redirect()->route('configs.index');

    }

}
