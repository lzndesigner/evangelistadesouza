<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Blogs\StoreRequest;
use App\Http\Requests\Admin\Blogs\UpdateRequest;

use App\Models\Blog;

class BlogsController extends Controller
{
    /**
     * Armazena uma nova instancia do model Blog
     *
     * @var \App\Blog
     */
    private $blogs;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
        $this->blogs = app(Blog::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $blogs = Blog::orderBy($request->input('sort', 'created_at'), 'ASC')->paginate();
        return view('admin.blogs.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blogs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $blog = new Blog;

        $foto_antiga = $blog->image;

        if(Storage::has($foto_antiga)){
          Storage::delete($foto_antiga);
        }

        $file = $request->file('image');

        if(!$file->isValid()) {
          abort(400, 'Imagem não encontrada ou não é válida');
        }

        $arquivo = $file->store('blogs');
        $blog->image = $arquivo;


        $blog->title = $request->title;
        $blog->slug = $request->slug;
        $blog->tags = $request->tags;
        $blog->body = $request->body;

        if(!$blog->save()) {
            session()->flash('messages.error', ['Houve um erro. Tente novamente!']);
            return redirect()->route('blogs.index');
        }

        session()->flash('messages.success', ['Postagem cadastrada com sucesso!']);
        return redirect()->route('blogs.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = Blog::findOrFail($id);
        return view('admin.blogs.show', compact('blog'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::findOrFail($id);
        return view('admin.blogs.edit', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $id)
    {
         $blog = $this->blogs->where('id', $id)->first();

        if(!$blog) {
            session()->flash('messages.error', ['Postagem não existe!']);
            return redirect()->route('blogs.index');
        }

      if(!Input::hasFile('image') == null){
        $foto_antiga = $blog->image;

        if(Storage::has($foto_antiga)){
          Storage::delete($foto_antiga);
        }

        $file = $request->file('image');

        if(!$file->isValid()) {
          abort(400, 'Imagem não encontrada ou não é válida');
        }

        $arquivo = $file->store('blogs');
        $blog->image = $arquivo;
      }

        $blog->title = $request->title;
        $blog->slug = $request->slug;
        $blog->tags = $request->tags;
        $blog->body = $request->body;
        $blog->save();

        session()->flash('messages.success', ['Postagem alterada com sucesso!']);
        return redirect()->route('blogs.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function destroy(Request $request, Blog $blog)
  {

    $blog->delete();

    return response(null, 204);
    //return response()->redirectToRoute('blogs.index');
  }
}
