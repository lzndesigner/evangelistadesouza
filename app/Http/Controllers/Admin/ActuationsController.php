<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Actuations\StoreRequest;
use App\Http\Requests\Admin\Actuations\UpdateRequest;

use App\Models\Actuation;

class ActuationsController extends Controller
{
    /**
     * Armazena uma nova instancia do model Actuation
     *
     * @var \App\Actuation
     */
    private $actuations;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
        $this->actuations = app(Actuation::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $actuations = Actuation::orderBy($request->input('sort', 'created_at'), 'ASC')->paginate();
        return view('admin.actuations.index', compact('actuations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.actuations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $actuation = new Actuation;

        $foto_antiga = $actuation->image;

        if(Storage::has($foto_antiga)){
          Storage::delete($foto_antiga);
        }

        $file = $request->file('image');

        if(!$file->isValid()) {
          abort(400, 'Imagem não encontrada ou não é válida');
        }

        $arquivo = $file->store('actuation');
        $actuation->image = $arquivo;


        $actuation->title = $request->title;
        $actuation->slug = $request->slug;
        $actuation->body = $request->body;

        if(!$actuation->save()) {
            session()->flash('messages.error', ['Houve um erro. Tente novamente!']);
            return redirect()->route('actuations.index');
        }

        session()->flash('messages.success', ['Atuação cadastrado com sucesso!']);
        return redirect()->route('actuations.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $actuation = Actuation::findOrFail($id);
        return view('admin.actuations.show', compact('actuation'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $actuation = Actuation::findOrFail($id);
        return view('admin.actuations.edit', compact('actuation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $id)
    {
         $actuation = $this->actuations->where('id', $id)->first();

        if(!$actuation) {
            session()->flash('messages.error', ['Atuação não existe!']);
            return redirect()->route('actuations.index');
        }

      if(!Input::hasFile('image') == null){
        $foto_antiga = $actuation->image;

        if(Storage::has($foto_antiga)){
          Storage::delete($foto_antiga);
        }

        $file = $request->file('image');

        if(!$file->isValid()) {
          abort(400, 'Imagem não encontrada ou não é válida');
        }

        $arquivo = $file->store('actuation');
        $actuation->image = $arquivo;
      }

        $actuation->title = $request->title;
        $actuation->slug = $request->slug;
        $actuation->body = $request->body;
        $actuation->save();

        session()->flash('messages.success', ['Atuação alterado com sucesso!']);
        return redirect()->route('actuations.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function destroy(Request $request, Actuation $actuation)
  {

    $actuation->delete();

    return response(null, 204);
    //return response()->redirectToRoute('blogs.index');
  }
}
