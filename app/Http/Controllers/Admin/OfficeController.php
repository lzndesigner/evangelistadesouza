<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Office\StoreRequest;
//use App\Http\Requests\Admin\Office\UpdateRequest;

use App\Models\Office;

class OfficeController extends Controller
{
    /**
     * Armazena uma nova instancia do model Office
     *
     * @var \App\Office
     */
    private $offices;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
      $this->offices = app(Office::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $office = Office::firstOrFail();
      return view('admin.offices.index', compact('office'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {

      $office = $this->offices->firstOrFail();

      if(!$office) {
        session()->flash('messages.error', ['Quem Somos não existe!']);
        return redirect()->route('office.index');
      }

      $office->page_description = $request->page_description;
      $office->save();

      session()->flash('messages.success', ['Quem Somos alterado com sucesso!']);
      return redirect()->route('office.index');

    }


    public function photos()
    {
      $office = \App\Models\Office::with('photos')->find(1);
      return view('admin.offices.photos', compact('office'));
    }

    public function upload()
    {
      $file = \Request::file('documento');
      $officeid = \Request::get('officeid');
      $storagePath = $file->store('office');
      $fileName = $storagePath;

      $fileModel = new \App\Models\OfficePhotos();
      $fileModel->name = $fileName;

      $office = \App\Models\Office::find($officeid);
      $office->photos()->save($fileModel);
    }

    public function download($officeId, $photoId)
    {
      $file = \App\Models\OfficePhotos::find($photoId);
      $storagePath = $file->store('office');
      return \Response::download($storagePath.'/'.$file->name);
      
    }
    public function destroy($officeId, $photoId)
    {
      $file = \App\Models\OfficePhotos::find($photoId);
      $storagePath = 'storage';
      $file->delete();
      unlink($storagePath.'/'.$file->name);

      session()->flash('messages.success', ['Foto deletada com sucesso!']);
      return redirect()->route('office-photos');
    }


  }