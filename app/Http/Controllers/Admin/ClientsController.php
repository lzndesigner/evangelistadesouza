<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Clients\StoreRequest;
use App\Http\Requests\Admin\Clients\UpdateRequest;

use App\Models\Client;

class ClientsController extends Controller
{
    /**
     * Armazena uma nova instancia do model client
     *
     * @var \App\client
     */
    private $clients;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
        $this->clients = app(Client::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $clients = Client::orderBy($request->input('sort', 'created_at'), 'ASC')->paginate();
        return view('admin.clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $client = new Client;

        $foto_antiga = $client->image;

        if(Storage::has($foto_antiga)){
          Storage::delete($foto_antiga);
        }

        $file = $request->file('image');

        if(!$file->isValid()) {
          abort(400, 'Imagem não encontrada ou não é válida');
        }

        $arquivo = $file->store('clientes');
        $client->image = $arquivo;


        $client->name = $request->name;
        $client->link = $request->link;
        $client->status = $request->status;

        if(!$client->save()) {
            session()->flash('messages.error', ['Houve um erro. Tente novamente!']);
            return redirect()->route('clients.index');
        }

        session()->flash('messages.success', ['Clientes cadastrado com sucesso!']);
        return redirect()->route('clients.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::findOrFail($id);
        return view('admin.clients.show', compact('client'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::findOrFail($id);
        return view('admin.clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $id)
    {
         $client = $this->clients->where('id', $id)->first();

        if(!$client) {
            session()->flash('messages.error', ['Cliente não existe!']);
            return redirect()->route('clients.index');
        }

      if(!Input::hasFile('image') == null){
        $foto_antiga = $client->image;

        if(Storage::has($foto_antiga)){
          Storage::delete($foto_antiga);
        }

        $file = $request->file('image');

        if(!$file->isValid()) {
          abort(400, 'Imagem não encontrada ou não é válida');
        }

        $arquivo = $file->store('clientes');
        $client->image = $arquivo;
      }

        $client->name = $request->name;
        $client->link = $request->link;
        $client->status = $request->status;
        $client->save();

        session()->flash('messages.success', ['Cliente alterado com sucesso!']);
        return redirect()->route('clients.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function destroy(Request $request, Client $client)
  {

    $client->delete();

    return response(null, 204);
    //return response()->redirectToRoute('blogs.index');
  }
}
