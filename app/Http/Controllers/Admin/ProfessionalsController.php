<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Professionals\StoreRequest;
use App\Http\Requests\Admin\Professionals\UpdateRequest;

use App\Models\Professional;

class ProfessionalsController extends Controller
{
    /**
     * Armazena uma nova instancia do model client
     *
     * @var \App\client
     */
    private $professionals;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
        $this->professionals = app(Professional::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $professionals = Professional::orderBy($request->input('sort', 'created_at'), 'ASC')->paginate();
        return view('admin.professionals.index', compact('professionals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.professionals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $professional = new Professional;

        $foto_antiga = $professional->image;

        if(Storage::has($foto_antiga)){
          Storage::delete($foto_antiga);
        }

        $file = $request->file('image');

        if(!$file->isValid()) {
          abort(400, 'Imagem não encontrada ou não é válida');
        }

        $arquivo = $file->store('professionals');
        $professional->image = $arquivo;


        $professional->name = $request->name;
        $professional->oab = $request->oab;
        $professional->body = $request->body;
        $professional->status = $request->status;

        if(!$professional->save()) {
            session()->flash('messages.error', ['Houve um erro. Tente novamente!']);
            return redirect()->route('professionals.index');
        }

        session()->flash('messages.success', ['Professional cadastrado com sucesso!']);
        return redirect()->route('professionals.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $professional = Professional::findOrFail($id);
        return view('admin.professionals.show', compact('professional'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $professional = Professional::findOrFail($id);
        return view('admin.professionals.edit', compact('professional'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $id)
    {
         $professional = $this->professionals->where('id', $id)->first();

        if(!$professional) {
            session()->flash('messages.error', ['Profissional não existe!']);
            return redirect()->route('professionals.index');
        }

      if(!Input::hasFile('image') == null){
        $foto_antiga = $professional->image;

        if(Storage::has($foto_antiga)){
          Storage::delete($foto_antiga);
        }

        $file = $request->file('image');

        if(!$file->isValid()) {
          abort(400, 'Imagem não encontrada ou não é válida');
        }

        $arquivo = $file->store('professionals');
        $professional->image = $arquivo;
      }

        $professional->name = $request->name;
        $professional->oab = $request->oab;
        $professional->body = $request->body;
        $professional->status = $request->status;
        $professional->save();

        session()->flash('messages.success', ['Profissional alterado com sucesso!']);
        return redirect()->route('professionals.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function destroy(Request $request, Professional $professional)
  {

    $professional->delete();

    return response(null, 204);
    //return response()->redirectToRoute('blogs.index');
  }
}
