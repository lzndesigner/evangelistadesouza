<?php

namespace App\Http\Requests\Admin\Professionals;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'name' => ['required', 'min:5'],
            'image' => ['required'],
            'oab' => ['required'],
        ];
    }

    public function attributes() {
        return [
            'name' => 'titulo',
            'image' => 'imagem',
            'oab' => 'OAB',
        ];
    }
    
    public function messages()
        {
            // voce refez as mensagens por que? Porque o tratamendo delas usa o name="" para informar o erro. exemplo, o campo config_name não foi preenchido, acho isso para o Cliente final meio estranho.
            // tem uma slução
            return [
                'name.required' => 'O título é necessário.',
                'image.required' => 'O imagem é necessário.',
                'oab.required' => 'O OAB é necessário.',
            ];
        }
}
