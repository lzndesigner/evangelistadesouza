<?php

namespace App\Http\Requests\Admin\Blogs;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'title' => ['required', 'min:5'],
            'slug' => ['required'], 
            'tags' => ['required'], 
            'image' => ['required'], 
            'body' => ['required', 'min:30'],
        ];
    }

    public function attributes() {
        return [
            'title' => 'titulo',
            'slug' => 'url amigável',
            'tags' => 'tags',
            'image' => 'imagem',
            'body' => 'conteudo'
        ];
    }
    
    public function messages()
        {
            // voce refez as mensagens por que? Porque o tratamendo delas usa o name="" para informar o erro. exemplo, o campo config_title não foi preenchido, acho isso para o Cliente final meio estranho.
            // tem uma slução
            return [
                'title.required' => 'O título é necessário.',
                'slug.required' => 'A URL Amigável é necessário.',
                'tags.required' => 'A TAG é necessário.',
                'image.required' => 'A Imagem é necessário.',
                'body.required'  => 'O conteúdo da página é necessário.',
            ];
        }
}
