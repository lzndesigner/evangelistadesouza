<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ReservationInterestCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
			'visualized' => $this->visualized,
            'message' => $this->message,
            'created_at' => $this->created_at->format('d-m-Y H:m'),
            'updated_at' => $this->updated_at->diffForHumans()
        ];
    }
}
