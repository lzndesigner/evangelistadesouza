let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.autoload({'jquery' : ['window.$', 'window.jQuery']});

mix.styles([
	'public/front/css/bootstrap.css',
	'public/front/css/style.css',
	'public/front/css/dark.css',
	'public/front/css/font-icons.css',
	'public/front/css/animate.css',
	'public/front/css/magnific-popup.css',
	'public/front/css/custom.css',
	'public/front/css/responsive.css'
	], 'public/front/css/app.css');

//mix.copy('public/backend/Fonts', 'public/backend/fonts');

mix.js('resources/assets/js/app.js', 'public/js').version();